﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Application
{
    public class AuditService : IAuditService
    {
        private readonly ITokenService _tokenSvc;
        private readonly IGenericRepository _rep;

        public AuditService(ITokenService tokenSvc, IGenericRepository rep)
        {
            _tokenSvc = tokenSvc;
            _rep = rep;
        }

        public async Task<CreateOutput> CreateAuditLog(InputData input, string userId)
        {

            var auditTable = $"AUDIT_{input.EntityName}";
            List<DbModel> insFields = new List<DbModel>();
            List<DbModel> inputFields = input.Fields == null ? new List<DbModel>() : input.Fields.ToList();
            
            try
            {
                // If the action is 'DELETE' set the 'st_delete_state' as default parameter
                if (input.Operation.ToUpper().Equals("DELETE"))
                {
                    var delParam = new List<DbModel>();
                    delParam.Add(new DbModel { ColumnName = "st_delete_state", Value = true });
                    inputFields = delParam;
                }

                // Create a log register for each modified column
                if (inputFields.Any())
                {
                    foreach (var item in inputFields)
                    {                  
                        insFields = SetAuditLogValues(input, userId, item.ColumnName, item.Value.ToString());
                        inputFields = insFields;
                        InputData newObj = new InputData(input.EntityId, input.EntityName, "AUDIT", inputFields, null);
                        await _rep.Create(newObj, userId);
                    }
                }

                return new CreateOutput
                {
                    Success = true
                };

            }
            catch (Exception ex)
            {

                return new CreateOutput
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<CreateOutput> CreateLogonLog(string token, string ip)
        {
            try
            {
                var user = _tokenSvc.GetUserFromToken(token);
                var insFields = SetLogonLogValues(user, ip);
                var input = new InputData (null, "LOGON_LOG", "LOG", insFields, null);
                await _rep.Create(input, user);


                return new CreateOutput
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {

                return new CreateOutput
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private List<DbModel> SetAuditLogValues(InputData input, string userId, string columnName, string newValue)
        {
            var fields = new List<DbModel>();
            var fmtDateNow = DataBaseHelper.DbDateFormat(DateTime.Now);


            fields.Add(new DbModel { ColumnName = "id_user", Value = Guid.Parse(userId) });
            fields.Add(new DbModel { ColumnName = "id_entity", Value = Guid.Parse(input.EntityId) });
            fields.Add(new DbModel { ColumnName = "id_operation", Value = DataBaseHelper.GetOperationIndex(input.Operation) });
            fields.Add(new DbModel { ColumnName = "nm_entity", Value = $"TB_{input.EntityName}" });
            fields.Add(new DbModel { ColumnName = "dt_createdon", Value = fmtDateNow });
            fields.Add(new DbModel { ColumnName = "nm_modifiedCol", Value = columnName });
            fields.Add(new DbModel { ColumnName = "ds_new_value", Value = newValue });

            return fields;
        }


        private List<DbModel> SetLogonLogValues(string userId, string ip)
        {
            var fields = new List<DbModel>();
            var fmtDateNow = DataBaseHelper.DbDateFormat(DateTime.Now);

            fields.Add(new DbModel { ColumnName = "id_user", Value = Guid.Parse(userId) });
            fields.Add(new DbModel { ColumnName = "dt_accesson", Value = fmtDateNow });
            fields.Add(new DbModel { ColumnName = "nr_ip", Value = ip });

            return fields;
        }
    }
}
