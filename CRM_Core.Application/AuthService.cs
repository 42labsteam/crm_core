﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Newtonsoft.Json;
using Novell.Directory.Ldap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Application
{
    public class AuthService : IAuthService
    {

        private readonly IUserService _userSvc;
        private readonly IGenericService _genSvc;


        public AuthService(IUserService userSvc, IGenericService genSvc)
        {
            _userSvc = userSvc;
            _genSvc = genSvc;
        }

        /// <summary>
        ///     Ad Authentication
        ///     Only registered users in the Ad
        /// </summary>
        /// <param name="auth"></param>
        /// <returns>Auth Result</returns>
        public async Task<AuthOutput> Auth(Auth auth)
        {
            try
            {
                if (!ObjectValidator.IsValidAuthObject(auth))
                {

                    return new AuthOutput
                    {
                        Success = false,
                        ErrorCode = 400,
                        ErrorMessage = ErrorCatcher.SetErrors()
                    };
                }

                using (var connection = new LdapConnection { SecureSocketLayer = false })
                {
                    string domain = DataBaseHelper.GetConnectionString("AdDomain");
                    string host = DataBaseHelper.GetConnectionString("AdHost");

                    string userDn = $"{auth.User}@{domain}";
                    connection.Connect(host, LdapConnection.DEFAULT_PORT);
                    connection.Bind(userDn, auth.Password);
                }


                var userName = await _userSvc.GetUserName(auth.User);
                var userId = await _userSvc.GetGetUserByLogon(auth.User);

                return new AuthOutput() { Success = true, UserName = userName, UserId = userId };



            }
            catch (LdapException ex)
            {

                if (ex.ResultCode == 49)
                {
                    return new AuthOutput() { Success = false, ErrorCode = 1010, ErrorMessage = "Invalid Credential" };
                }
                if (ex.ResultCode == 91)
                {
                    var password = await _userSvc.GetCryptedPassword(auth.User);

                    if (password == null)
                    {
                        return new AuthOutput() { Success = false, ErrorCode = 401, ErrorMessage = "Error while extracting the user from token" };
                    }

                    var output = new BaseOutput();

                    if (string.IsNullOrEmpty(password))
                    {
                        return new AuthOutput() { Success = false, ErrorCode = 1020, ErrorMessage = "Not authorized user" };
                    }
                    else
                    {
                        output = await _userSvc.AuthUser(auth.User, password);

                        if (output.Success)
                        {
                            var userName = await _userSvc.GetUserName(auth.User);
                            var userId = await _userSvc.GetGetUserByLogon(auth.User);
                            return new AuthOutput() { Success = true, UserName = userName, UserId = userId };
                        }
                        return new AuthOutput() { Success = false, ErrorCode = output.ErrorCode, ErrorMessage = ErrorCatcher.SetErrors() };
                    }
                }

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Auth", $"{ex.Message} {innerEx}");
                return new AuthOutput() { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() };

            }
            catch (Exception ex)
            {

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Auth", $"{ex.Message} {innerEx}");
                return new AuthOutput() { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() };
            }


        }



        private void Createlog(string ex)
        {
            try
            {
                CreateDirectory();

                string path = $@"..\..\temp\log\teste_{Guid.NewGuid()}.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(ex);
                    }
                }
            }
            catch (Exception e)
            {

                Console.WriteLine("The log proccess failed.");
            }
        }

        private void CreateDirectory()
        {
            string path = @"..\..\temp\log\";
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(path))
                {
                    Console.WriteLine("That path exists already.");
                    return;
                }

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path);
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path));


            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
            finally { }

        }
    }
}
