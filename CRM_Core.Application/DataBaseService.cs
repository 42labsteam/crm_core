﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace CRM_Core.Application
{
    public class DataBaseService : IDataBaseService
    {
        private DbTableRepository rep = new DbTableRepository();
        private IGenericRepository _genRep;

        public DataBaseService(IGenericRepository genRep)
        {
            _genRep = genRep;
        }

        public DataBaseService() { }

        public async Task<BaseOutput> CreateTable(DbTableModel toCreate, string script = null)
        {

            try
            {
                if (string.IsNullOrEmpty(script))
                {
                    if (!ObjectValidator.IsValidTabelModel(toCreate))
                    {
                        return new BaseOutput
                        {
                            Success = false,
                            ErrorCode = 400,
                            ErrorMessage = ErrorCatcher.SetErrors()
                        };
                    }
                }

                var wasCreated = await rep.CreateTable(toCreate, script);

                if (!wasCreated)
                    return new BaseOutput
                    {
                        Success = false,
                        ErrorCode = 500,
                        ErrorMessage = "Error while creating the database table"
                    };

                //List<DbModel> fks = toCreate.ForeignKeys ?? new List<DbModel>();

                var auditTable = await CreateAuditTable(toCreate.EntityName);

                if (!auditTable.Success)
                    return auditTable;

                return new BaseOutput
                {
                    Success = true,
                    ErrorMessage = ErrorCatcher.SetErrors()
                };

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "CreateTable", $"{ex.Message} {innerEx}");
                return new BaseOutput
                {
                    Success = false,
                    ErrorCode = 400,
                    ErrorMessage = ErrorCatcher.SetErrors()
                };
            }

        }

        public async Task<BaseOutput> CreateAuditTable(string entityName)
        {
            try
            {
                var tableModel = new DbTableModel
                {
                    Fields = SetAuditFields(),
                    EntityName = $"AUDIT_{entityName}"
                };

                var script = DataBaseHelper.WriteCreateTableScript(tableModel);
                var newTable = await CreateTable(tableModel);
                await rep.AddFK(entityName, "user");
                await rep.AddFK(entityName, "operation");

                return newTable;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "CreateAuditTable", $"{ex.Message} {innerEx}");
                return new BaseOutput
                {
                    Success = false,
                    ErrorCode = 400,
                    ErrorMessage = ErrorCatcher.SetErrors()
                };
            }

        }


        public async Task<RetrieveOutput> GetAllTableNames()
        {
            try
            {
                var tableNames = new List<dynamic>();
                string fmtTableName = string.Empty;

                var sqlQuery = @"
                                SELECT * 
                                FROM TB_ENTITY";

                var table = await _genRep.QuerySelect(sqlQuery);

                foreach (DataRow row in table.Rows)
                {
                    var tableName = new
                    {
                        idEntity = row["id_entity"].ToString(),
                        ptLabel = row["nm_pt_label"].ToString(),
                        dbName = row["nm_db_name"].ToString()
                    };

                    if (!tableName.dbName.Contains("log") &&
                        !tableName.dbName.Contains("audit") &&
                        !tableName.dbName.Contains("entity_dll") &&
                        !tableName.dbName.Contains("invoke_dll") &&
                        !tableName.ptLabel.Contains("log") &&
                        !tableName.ptLabel.Contains("audit") &&
                        !tableName.ptLabel.Contains("entity_dll") &&
                        !tableName.ptLabel.Contains("invoke_dll"))
                    {
                        tableNames.Add(tableName);
                    }
                }

                return new RetrieveOutput { Success = true, Content = tableNames };
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetAllTableNames", $"{ex.Message} {innerEx}");
                return new RetrieveOutput { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() };
            }
        }


        private List<DbModel> SetAuditFields()
        {
            List<DbModel> fields = new List<DbModel>();
            fields.Add(new DbModel { ColumnName = "ID_USER", Value = "UUID", isRequired = true });
            fields.Add(new DbModel { ColumnName = "ID_ENTITY", Value = "UUID", isRequired = true });
            fields.Add(new DbModel { ColumnName = "NM_ENTITY", Value = "VARCHAR", isRequired = true });
            fields.Add(new DbModel { ColumnName = "ID_OPERATION", Value = "INT", isRequired = true });
            fields.Add(new DbModel { ColumnName = "DT_CREATEDON", Value = "TIMESTAMP", isRequired = true });

            return fields;
        }

    }
}