﻿using CRM_Core.Domain.Enums;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Caching.Memory;
using System.Threading.Tasks;
using System.Data;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Hosting;
using System.Threading;

namespace CRM_Core.Application
{
    public class FileService : IFileService, IHostedService, IDisposable
    {
        private IFileRepository _fileRep;
        private IMemoryCache _cache;
        private IGenericRepository _genRep;
        private ITokenService _tokenSvc;
        private const string CACHE_KEY = "DllKey";
        private List<DownloadDllOutput> dlls;

        public FileService(IConfiguration config, IFileRepository fileRep, IMemoryCache cache, IGenericRepository genRep, ITokenService tokenSvc)
        {
            _fileRep = fileRep;
            _cache = cache;
            _genRep = genRep;
            _tokenSvc = tokenSvc;
        }

        public async Task<BaseOutput> UploadDllFile(PluginModel input, string action)
        {
            try
            {
                var user = _tokenSvc.GetUserFromToken(input.Token);
                var result = await _fileRep.UploadDllFile(input, action, user);

                if (!result.Success)
                {
                    result.ErrorMessage = ErrorCatcher.SetErrors();
                    return result;
                }


                _cache.Remove(CACHE_KEY);
                var files = await GetAllDllFiles();

                if (files == null)
                {
                    result.ErrorMessage = ErrorCatcher.SetErrors();
                    result.ErrorCode = 400;
                    result.Success = false;
                    return result;
                }

                return result;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "UploadDllFile", $"{ex.Message} {innerEx}");
                return new BaseOutput { Success = false, ErrorMessage = ErrorCatcher.SetErrors() };
            }
        }


        public async Task<List<DownloadDllOutput>> GetAllDllFiles()
        {
            try
            {
                var files = new List<DownloadDllOutput>();
                if (!_cache.TryGetValue<List<DownloadDllOutput>>(CACHE_KEY, out dlls))
                {
                    var table = await _fileRep.GetAllDllFiles();
                    foreach (DataRow row in table.Rows)
                    {
                        files.Add(
                            new DownloadDllOutput
                            {
                                Assembly = Assembly.Load(Convert.FromBase64String(row["ds_data"].ToString())),
                                EntityName = row["nm_entity_name"].ToString(),
                                FileName = row["nm_file"].ToString(),
                                Operation = Convert.ToInt32(row["id_operation"].ToString()),
                                InvokeAt = Convert.ToInt32(row["id_invoke_dll"].ToString()),
                                Success = true
                            }
                            );
                        dlls = files;
                        _cache.Set(CACHE_KEY, dlls);
                    }
                }


                return dlls;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetAllDllFiles", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            GetAllDllFiles().ConfigureAwait(false);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            try
            {

            }
            catch (Exception ex)
            {

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Dispose", $"{ex.Message} {innerEx}");

            }

        }

        public async Task<List<DownloadDllOutput>> GetDllsByOperation(string operation, string entityName, int invoteAt)
        {
            try
            {
                var dlls = await GetAllDllFiles();
                var resultList = new List<DownloadDllOutput>();
                if (dlls != null)
                    resultList = dlls.Where(x => x.EntityName.ToLower().Equals(entityName.ToLower()) && x.Operation == DataBaseHelper.GetOperationIndex(operation) && x.InvokeAt == invoteAt).ToList();

                return resultList;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetDllsByOperation", $"{ex.Message} {innerEx}");
                return null;
            }

        }

        //public Task<BaseOutput> DeleteDllFile(InputData input, string userId)
        //{
        //    try
        //    {
        //        var result = _genRep.Delete(input, );
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}
    }
}
