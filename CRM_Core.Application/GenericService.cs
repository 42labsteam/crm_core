﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X.PagedList;

namespace CRM_Core.Application
{
    public class GenericService : IGenericService
    {
        private readonly IGenericRepository _rep;
        private readonly ITokenService _tokenSvc;
        private readonly IAuditService _auditSvc;

        public GenericService(IGenericRepository rep, ITokenService tokenSvc, IAuditService auditSvc)
        {
            _rep = rep;
            _tokenSvc = tokenSvc;
            _auditSvc = auditSvc;
        }

        public GenericService()
        {

        }

        public async Task<CreateOutput> Create(InputData input)
        {
            try
            {
                input.AddOperation("create");
                List<DbModel> data = input.Fields.ToList() ?? new List<DbModel>();
                var hasListValue = data.Where(x => x.Value.ToString().Contains("[") && x.Value.ToString().Contains("]")) ?? new List<DbModel>();
                var filteredList = new List<DbModel>();

                if (hasListValue.Any())
                {
                    var list = data.Where(x => !x.Value.ToString().Contains("[") && !x.Value.ToString().Contains("]"));
                    filteredList = list.ToList();
                }

                var newInput = new InputData(null, input.EntityName, input.Operation ?? "create", hasListValue.Any() ? filteredList : data, input.Token);

                var user = _tokenSvc.GetUserFromToken(newInput.Token);

                if (user == null)
                {
                    ErrorCatcher.AddError("Error while extracting the user from token");
                    return new CreateOutput { ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors(), Success = false };
                }


                if (input.EntityName.ToLower().Equals("user"))
                {
                    var cryptedPassword = Crypto.CryptPassword(input);

                    if (!cryptedPassword.Success)
                        return new CreateOutput { Success = false, ErrorMessage = cryptedPassword.ErrorMessage, ErrorCode = 500 };

                }

                var newEntity = await _rep.Create(newInput, user);

                if (!newEntity.Success)
                {
                    newEntity.ErrorMessage = ErrorCatcher.SetErrors();
                    return newEntity;
                }

                var auditInput = new InputData(newEntity.EntityId, input.EntityName, "create", hasListValue.Any() ? filteredList : data, input.Token);
                var audit = await _auditSvc.CreateAuditLog(auditInput, user);

                if (!audit.Success)
                {
                    audit.ErrorMessage = ErrorCatcher.SetErrors();
                    return audit;
                }

                return new CreateOutput
                {
                    Success = true,
                    EntityId = newEntity.EntityId
                };


            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Create", $"{ex.Message} {innerEx}");
                return new CreateOutput
                {
                    Success = false,
                    ErrorMessage = ErrorCatcher.SetErrors(),
                    ErrorCode = 500
                };
            }

        }

        public async Task<BaseOutput> Delete(InputData input)
        {
            try
            {
                var updateInput = new InputData(input.EntityId, input.EntityName, input.Operation ?? "delete", input.Fields, input.Token);

                var user = _tokenSvc.GetUserFromToken(updateInput.Token);

                if (user == null)
                {
                    ErrorCatcher.AddError("Error while extracting the user from token");
                    return new CreateOutput { ErrorCode = 400, ErrorMessage = ErrorCatcher.SetErrors(), Success = false };
                }

                bool isDeleted = await _rep.Delete(updateInput, user);

                if (!isDeleted && ErrorCatcher.ErrorList.Any())
                {
                    return new BaseOutput { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() };
                }

                var audit = await _auditSvc.CreateAuditLog(updateInput, user);

                if (!audit.Success)
                {
                    return new BaseOutput
                    {
                        ErrorMessage = ErrorCatcher.SetErrors(),
                        Success = false,
                        ErrorCode = 400
                    };
                }

                if (!isDeleted)
                {
                    ErrorCatcher.AddError("No data found");

                    return new BaseOutput
                    {
                        ErrorMessage = ErrorCatcher.SetErrors(),
                        Success = false,
                        ErrorCode = 400
                    };
                }

                return new BaseOutput { Success = true };

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Delete", $"{ex.Message} {innerEx}");
                return new BaseOutput
                {

                    Success = false,
                    ErrorMessage = ErrorCatcher.SetErrors(),
                    ErrorCode = 500
                };
            }

        }

        public async Task<BaseOutput> Update(InputData input)
        {
            try
            {
                var deleteInput = new InputData(input.EntityId, input.EntityName, input.Operation ?? "update", input.Fields, input.Token);

                var entity = GetSingle(deleteInput);

                if (string.IsNullOrEmpty(entity.Result.EntityId))
                {
                    return new BaseOutput
                    {
                        Success = false,
                        ErrorMessage = "No data found"
                    };
                }

                var user = _tokenSvc.GetUserFromToken(deleteInput.Token);

                if (input.EntityName.ToLower().Equals("user"))
                {
                    var cryptedPassword = Crypto.CryptPassword(input);

                    if (!cryptedPassword.Success)
                        return cryptedPassword;
                }

                var result = await _rep.Update(deleteInput, user);

                var audit = await _auditSvc.CreateAuditLog(deleteInput, user);

                if (!audit.Success)
                {
                    return new BaseOutput
                    {
                        ErrorMessage = audit.ErrorMessage,
                        Success = false
                    };
                }

                return result;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Update", $"{ex.Message} {innerEx}");
                return new BaseOutput
                {
                    Success = false,
                    ErrorMessage = ErrorCatcher.SetErrors(),
                    ErrorCode = 500
                };
            }

        }

        public async Task<GetSingleOutput> GetSingle(InputData obj)
        {
            try
            {
                obj.AddOperation("retrieve");


                var result = await _rep.GetSingle(obj, null);

                if (string.IsNullOrEmpty(result.EntityId))
                {
                    ErrorCatcher.AddError("No data found");
                    return new GetSingleOutput
                    {
                        Success = false,
                        ErrorMessage = ErrorCatcher.SetErrors(),
                        ErrorCode = 400
                    };

                }

                return new GetSingleOutput { Success = true, Content = result.Content, EntityId = result.EntityId };

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetSingle", $"{ex.Message} {innerEx}");
                return new GetSingleOutput
                {
                    Success = false,
                    ErrorMessage = ErrorCatcher.SetErrors(),
                    ErrorCode = 500
                };
            }
        }


        public async Task<RetrieveOutput> GetAll(RetrieveModel input)
        {

            if (input == null)
            {
                return new RetrieveOutput
                {
                    Success = false,
                    ErrorMessage = "The object is null"
                };
            }


            List<DbModel> filter = new List<DbModel>();
            var fields = input.Fields ?? new List<DbModel>();
            input.Filters = input.Filters ?? new List<DbFilterModel>();
            input.JoinTables = input.JoinTables ?? new List<DbJoinTableModel>();
            bool hasColId = false;

            try
            {
                RetrieveModel obj = new RetrieveModel(input.EntityId, input.EntityName, "GET_ALL", fields, input.Token);
                obj.Filters = input.Filters ?? new List<DbFilterModel>();
                obj.JoinTables = input.JoinTables ?? new List<DbJoinTableModel>();



                if (!ObjectValidator.IsValidDbJoinTableModel(obj))
                {
                    return new RetrieveOutput
                    {
                        Success = false,
                        ErrorCode = 400,
                        ErrorMessage = ErrorCatcher.SetErrors()
                    };
                }

                if (!ObjectValidator.IsValidDbFilterObject(obj))
                {
                    return new RetrieveOutput
                    {
                        Success = false,
                        ErrorCode = 400,
                        ErrorMessage = ErrorCatcher.SetErrors()
                    };
                }

                if (obj.Fields != null)
                {
                    filter = obj.Fields.ToList();
                    hasColId = filter.Find(x => x.ColumnName.Equals($"id_{obj.EntityName.ToLower()}")) != null ? true : false;
                }
                else
                {
                    filter = new List<DbModel>();
                }

                RetrieveModel filerObj = new RetrieveModel(input.EntityId, input.EntityName, "GET_ALL", filter, input.Token);
                filerObj.Filters = input.Filters ?? new List<DbFilterModel>();
                filerObj.JoinTables = input.JoinTables ?? new List<DbJoinTableModel>();

                var result = await GetPaginationList(filerObj);

                if (!result.Success)
                {
                    result.ErrorMessage = ErrorCatcher.SetErrors();
                    return result;
                }

                if (result.Content == null)
                {
                    ErrorCatcher.AddError("No data found");
                    return new RetrieveOutput
                    {
                        Success = false,
                        ErrorMessage = ErrorCatcher.SetErrors(),
                        ErrorCode = 400
                    };

                }

                result.Success = true;

                return result;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetAll", $"{ex.Message} {innerEx}");
                return new RetrieveOutput
                {
                    Success = false,
                    ErrorMessage = ErrorCatcher.SetErrors(),
                    ErrorCode = 500
                };
            }

        }

        private async Task<RetrieveOutput> GetPaginationList(RetrieveModel input)
        {
            try
            {
                var result = await _rep.GetAll(input);

                if (!result.Success)
                {
                    result.ErrorMessage = ErrorCatcher.SetErrors();
                    return result;
                }

                var pageNumber = input.Page ?? 1;
                var json = JsonConvert.SerializeObject(result.Content);
                List<Dictionary<string, object>> convertedJson = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json);

                IEnumerable<Dictionary<string, object>> query = convertedJson.AsQueryable().Cast<Dictionary<string, object>>();

                result.Content = query.ToPagedList(pageNumber, 250);

                return result;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetPaginationList", $"{ex.Message} {innerEx}");
                return new RetrieveOutput
                {
                    Success = false,
                    ErrorCode = 500
                };
            }
        }


    }
}
