﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Application
{
    public class PrivilegeService : IPrivilegeService
    {
        private List<Privilege> priv;

        private ITokenService _tokenSvc;
        private IGenericRepository _rep;
        private IMemoryCache _cache;
        private const string CACHE_KEY = "PrivilegeKey";

        public PrivilegeService(IMemoryCache cache, IGenericRepository rep, ITokenService tokenSvc)
        {
            _cache = cache;
            _rep = rep;
            _tokenSvc = tokenSvc;
        }


        public async Task<CreateOutput> ValidatePrivilege(InputData input)
        {
            var result = new CreateOutput();

            try
            {

                result = await GetPrivilege(input);
                return result;
            }
            catch (Exception ex)
            {
                return new CreateOutput { Success = false, ErrorCode = 1000, ErrorMessage = ex.Message };
            }


        }

        /// <summary>
        /// Retrieve all registered privileges
        /// </summary>
        /// <returns></returns>
        private async Task<DataTable> CreateUserRoleDataTable()
        {

            try
            {
                string query = "SELECT id_user, id_role FROM public.\"tb_user_role\";";
                return await _rep.QuerySelect(query);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private List<UserRole> RetrieveUserRoles(DataTable table)
        {
            try
            {

                List<UserRole> userRoles = new List<UserRole>();



                foreach (DataRow r in table.Rows)
                {
                    userRoles.Add(new UserRole()
                    {
                        RoleId = (Guid)r["id_role"],
                        UserId = (Guid)r["id_user"]
                    });
                }

                return userRoles;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private async Task<List<Privilege>> GetPrivileges(DataTable tUserRoles, List<UserRole> userRoles)
        {
            try
            {
                List<Privilege> privileges = new List<Privilege>();

                string query = @"
                                    SELECT priv.id_role, ent.nm_db_name, priv.permissions 
                                    FROM tb_privilege priv 
                                    INNER JOIN tb_entity ent
                                    ON (priv.id_entity = ent.id_entity);";

                tUserRoles = await _rep.QuerySelect(query);


                foreach (DataRow row in tUserRoles.Rows)
                {
                    foreach (UserRole userRole in userRoles)
                    {
                        Guid roleid = (Guid)row["id_role"];

                        if (userRole.RoleId == roleid)
                        {
                            privileges.Add(new Privilege()
                            {
                                UserId = userRole.UserId,
                                Entity = row["nm_db_name"].ToString(),
                                Permissions = row["permissions"].ToString()
                            });
                        }

                    }
                }

                return privileges;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private async Task<CreateOutput> GetPrivilege(InputData input)
        {
            //if (input.EntityName.ToLower().Equals("privilege") && !input.Operation.ToLower().Equals("retrieve")) {
            //    _cache.Remove(CACHE_KEY);
            //}

            //if (!_cache.TryGetValue<List<Privilege>>(CACHE_KEY, out priv))
            //{
            var table = await CreateUserRoleDataTable();
            var roles = RetrieveUserRoles(table);
            priv = await GetPrivileges(table, roles);

            //_cache.Set(CACHE_KEY, priv);
            //}


            var userId = _tokenSvc.GetUserFromToken(input.Token);

            var y = priv.Where(x => x.Entity.Equals("ominie_account"));

            int index = DataBaseHelper.GetOperationIndex(input.Operation);

            if (index > -1)
            {
                foreach (Privilege privilege in priv)
                {
                    if (userId == privilege.UserId.ToString() && input.EntityName.ToLower() == privilege.Entity)
                    {
                        if (privilege.Permissions.Substring(index, 1) == "1")
                        {
                            return new CreateOutput { Success = true, ErrorCode = 0 };
                        }
                    }
                }

                return new CreateOutput { Success = false, ErrorCode = 405, ErrorMessage = "Permission denied" };
            }
            else
            {
                return new CreateOutput { Success = false, ErrorCode = 2000, ErrorMessage = "Operation not implemented." };
            }
        }
    }
}
