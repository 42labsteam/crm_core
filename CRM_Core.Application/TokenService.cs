﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Application
{
    public class TokenService : ITokenService
    {

        private UserRepository userRep = new UserRepository();

        public async Task<TokenResult> GenerateToken(string logon)
        {
            try
            {
                Token t = new Token
                {
                    Id = await userRep.GetUserByLogon(logon),
                    ExpiresOn = DataBaseHelper.DbDateFormat(DateTime.Now.AddMinutes(720))
                };

                string token = Crypto.EncryptString(JsonConvert.SerializeObject(t));

                return new TokenResult { Success = true, Token = token, ErrorCode = 0, ExpiresOn = t.ExpiresOn, ErrorMessage = ErrorCatcher.SetErrors() };
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GenerateToken", $"{ex.Message} {innerEx}");
                return new TokenResult { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() };
            }
        }

        public TokenResult CheckToken(string token)
        {
            try
            {
                Token t = JsonConvert.DeserializeObject<Token>(Crypto.DecryptString(token));

                if (t.ExpiresOn < DateTime.Now)
                {
                    return new TokenResult
                    {
                        Success = false,
                        ErrorCode = 1000,
                        ErrorMessage = "Expired Token"
                    };
                }

                return new TokenResult { Success = true, ErrorCode = 0, ExpiresOn = t.ExpiresOn };
            }
            catch
            {
                return new TokenResult
                {
                    Success = false,
                    ErrorCode = 1300,
                    ErrorMessage = "Invalid Token"
                };
            }
        }

        public string GetUserFromToken(string token)
        {
            try
            {
                Token t = JsonConvert.DeserializeObject<Token>(Crypto.DecryptString(token));
                return t.Id;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetUserFromToken", $"{ex.Message} {innerEx}");
                return null;
            };


        }

        public DateTime GetTokenExpires(string token)
        {
            try
            {
                Token t = JsonConvert.DeserializeObject<Token>(Crypto.DecryptString(token));
                return t.ExpiresOn;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetTokenExpires", $"{ex.Message} {innerEx}");
                return default(DateTime);
            };
        }
    }
}
