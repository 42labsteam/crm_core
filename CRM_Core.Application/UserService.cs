﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Application
{
    public class UserService : IUserService
    {
        private IUserRepository _rep;
        private IGenericRepository _genRep;
        private ITokenService _tokenSvc;


        public UserService(IUserRepository rep, IGenericRepository genRep, ITokenService tokenSvc)
        {
            _rep = rep;
            _genRep = genRep;
            _tokenSvc = tokenSvc;
        }

        public UserService() { }

        public async Task<BaseOutput> AuthUser(string userName, string password)
        {
            try
            {
                var userPassword = await GetCryptedPassword(userName.ToLower());

                if (!string.IsNullOrEmpty(userPassword))
                {
                    var userId = await _rep.AuthUser(userName.ToLower(), userPassword);

                    if (string.IsNullOrEmpty(userId))
                    {
                        ErrorCatcher.AddError("Incorrect username/password");
                        return new BaseOutput { Success = false, ErrorCode = 400, ErrorMessage = ErrorCatcher.SetErrors() };
                    }
                }
                else
                {
                    ErrorCatcher.AddError("Incorrect username/password");
                    return new BaseOutput { Success = false, ErrorCode = 400, ErrorMessage = ErrorCatcher.SetErrors() };
                }

                return new BaseOutput { Success = true, ErrorMessage = ErrorCatcher.SetErrors() };

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "AuthUser", $"{ex.Message} {innerEx}");
                return new BaseOutput { Success = false, ErrorCode = 400, ErrorMessage = ErrorCatcher.SetErrors() };
            }
        }

        public async Task<string> GetCryptedPassword(string logon)
        {
            string cryptoPassword = string.Empty;

            try
            {
                var userId = await GetGetUserByLogon(logon);

                if (string.IsNullOrEmpty(userId))
                    return null;

                var result = await _genRep.GetSingle(new InputData(userId, "USER", null, null, null), null);
                var user = new RouteValueDictionary(result.Content);
                if (user.TryGetValue("ds_password", out object value))
                    cryptoPassword = value.ToString();

                return cryptoPassword;


            }
            catch (Exception ex)
            {

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetCryptedPassword", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public string GetDecryptedPassword(string cryptoPassword)
        {
            try
            {
                var password = Crypto.DecryptString(cryptoPassword);
                return password;
            }
            catch (Exception ex)
            {

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetDecryptedPassword", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public async Task<string> GetFirstUser()
        {
            string userId = string.Empty;
            List<DbModel> users = new List<DbModel>();
            try
            {
                var table = await _rep.GetAllUsers();

                foreach (DataRow row in table.Rows)
                {
                    users.Add(new DbModel { ColumnName = row["id_user"].ToString(), Value = Convert.ToDateTime(row["dt_createdon"].ToString()) });
                }

                var orderList = users.OrderByDescending(x => x.Value);

                return null;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetFirstUser", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public async Task<string> GetGetUserByLogon(string userName)
        {
            try
            {
                var userId = await _rep.GetUserByLogon(userName);

                if (string.IsNullOrEmpty(userId))
                    return null;

                return userId;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetGetUserByLogon", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public async Task<string> GetUserName(string logon)
        {
            try
            {
                var name = await _rep.GetUserName(logon);

                if (string.IsNullOrEmpty(name))
                    return "";

                return name;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetUserName", $"{ex.Message} {innerEx}");
                return null;
            }
        }


        public async Task<BaseOutput> UpdatePasswordFromAdAuth(Auth auth)
        {
            try
            {
                var password = await GetCryptedPassword(auth.User);
                var userId = await GetGetUserByLogon(auth.User);

                if (string.IsNullOrEmpty(password))
                {
                    var newPassword = Crypto.EncryptString(auth.Password);
                    var passwordUpdated = await _genRep.Update(new InputData
                    (userId, "update", "user", new List<DbModel> { new DbModel { ColumnName = "ds_password", Value = newPassword } }, auth.Token), userId);

                    if (!passwordUpdated.Success)
                    {
                        ErrorCatcher.AddError("An error occurred while saving password to database");
                        return new BaseOutput { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() };
                    }
                }

                return new BaseOutput { Success = true };
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "UpdatePasswordFromAdAuth", $"{ex.Message} {innerEx}");
                return new BaseOutput { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() };
            }
        }
    }
}
