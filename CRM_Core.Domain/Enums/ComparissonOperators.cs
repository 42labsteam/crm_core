﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Enums
{
    public enum ComparisonOperators
    {
        EQUAL_TO = 1,
        GREATHER_THAN = 2,
        LESS_THAN = 3,
        GREATHER_THAN_OR_EQUAL_TO = 4,
        LESS_THAN_OR_EQUAL_TO = 5,
        NOT_EQUAL_TO = 6
    }
}
