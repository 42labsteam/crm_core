﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Enums
{
    public enum JoinTypes
    {
        LEFT_JOIN = 1,
        RIGHT_JOIN = 2,
        INNER_JOIN = 3,
        FULL_OUTER_JOIN = 4
    }
}
