﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Enums
{
    public enum LogicalOperators
    {
        AND = 1,
        BETWEEN = 2,
        LIKE = 3,
        NOT = 4,
        OR = 5,
        IS_NULL = 7
    }
}
