﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Enums
{
    public enum OrderByDirection
    {
        ASC = 1,
        DESC = 2
    }
}
