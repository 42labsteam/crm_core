﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class Audit : BaseOutput
    {
        public Guid UserId { get; set; }
        public Guid EntityId { get; set; }
        public string Entity { get; set; }
        public string Operation { get; set; }
        public IEnumerable<DbModel> ModifiedFields { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
