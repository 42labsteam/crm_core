﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class Auth
    {
        [Required]
        public string User { get; set; }
        [Required]
        public string Password { get; set; }
        public string Token { get; set; }

        public Auth(string user, string password)
        {
            User = user;
            Password = password;
        }


    }
}
