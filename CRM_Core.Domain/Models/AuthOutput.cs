﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class AuthOutput : BaseOutput
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
        public DateTime ExpiresOn { get; set; }

    }
}
