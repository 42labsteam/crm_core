﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class BaseInput
    {
        [Required]
        public string Token { get; set; }
    }
}
