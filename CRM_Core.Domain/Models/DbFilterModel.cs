﻿using CRM_Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class DbFilterModel : DbModel
    {
        public string EntityName { get; set; }
        public LogicalOperators ConcatOperator { get; set; }
        public ComparisonOperators Comparison { get; set; }
        public LogicalOperators LogicalOperator { get; set; }
        public object BetweenValue { get; set; }
        public object BetweenValue2 { get; set; }


        
    }
}
