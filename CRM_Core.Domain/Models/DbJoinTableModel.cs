﻿using CRM_Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class DbJoinTableModel
    {

        public string JoinedTable { get; set; }
        public string RightJoinId { get; set; }
        public string LeftJoinId { get; set; }
        public string RightJoinTable { get; set; }
        public string LeftJoinTable { get; set; }
        public JoinTypes JoinType { get; set; }
        public IEnumerable<DbModel> JoinColumns { get; set; }
    }
}
