﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class DbModel
    {
        public string ColumnName { get; set; }
        public object Value { get; set; }
        public bool isRequired { get; set; }

    }
}
