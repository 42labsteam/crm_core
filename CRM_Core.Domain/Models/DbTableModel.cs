﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class DbTableModel 
    {
        public string EntityName { get; set; }
        public IEnumerable<DbModel> Fields { get; set; }
        public IEnumerable<DbModel> ForeignKeys { get; set; }
    }
}
