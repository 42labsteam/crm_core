﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class DownloadDllOutput : BaseOutput
    {
        public Assembly Assembly { get; set; }
        public string FileName { get; set; }
        public string EntityName { get; set; }
        public int InvokeAt { get; set; }
        public int Operation { get; set; }
    }
}
