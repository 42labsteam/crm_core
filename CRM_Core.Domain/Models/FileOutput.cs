﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class FileOutput : BaseOutput
    {
        public string FileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string DirectoryPath { get; set; }
        public byte[] Content { get; set; }
    }
}
