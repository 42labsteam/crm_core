﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class GenerateAssemblyModel : BaseInput
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string DllName { get; set; }
        [Required]
        public int InvokeAt { get; set; }
        [Required]
        public string EntityName { get; set; }
        [Required]
        public string Operation { get; set; }
    }
}
