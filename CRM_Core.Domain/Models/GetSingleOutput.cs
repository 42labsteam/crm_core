﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class GetSingleOutput : BaseOutput
    {
        public string EntityId { get; set; }
        public dynamic Content { get; set; }
    }
}
