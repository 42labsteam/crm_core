﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class InputData : BaseInput
    {
        public string EntityId { get; private set; }
        [Required]
        public string EntityName { get; private set; }
        public string Operation { get; private set; }
        public IEnumerable<DbModel> Fields { get; private set; }

        public InputData(string entityId, string entityName, string operation, IEnumerable<DbModel> fields, string token) : base()
        {
            EntityId = entityId;
            EntityName = entityName;
            Operation = operation;
            Fields = fields;
            base.Token = token;
        }

      public void AddOperation(string operation)
        {
            Operation = operation;
        }

    }
}
