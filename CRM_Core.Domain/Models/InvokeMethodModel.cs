﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class InvokeMethodModel
    {
        public string AssemblyName { get; set; }
        public string NameSpace { get; set; }
        public string ClassName { get; set; }
        public string Method { get; set; }
        public object[] Parameters { get; set; }
    }
}
