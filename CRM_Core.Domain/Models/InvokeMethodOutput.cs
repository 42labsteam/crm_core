﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class InvokeMethodOutput : BaseOutput
    {
        public object Result { get; set; }
    }
}
