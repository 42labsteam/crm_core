﻿using CRM_Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class OrderBy
    {
        public string ColumnName { get; set; }
        public string TableName { get; set; }
        public OrderByDirection Direction { get; set; }


    }
}
