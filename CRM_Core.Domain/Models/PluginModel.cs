﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class PluginModel : BaseInput
    {
        [Required]
        public string Base64StringDll { get; set; }
        [Required]
        public string EntityName { get; set; }
        [Required]
        [Range(1,2, ErrorMessage = "Only options 1 or 2 are allowed")]
        public int InvokeAt { get; set; }
        [Required]
        public string Operation { get; set; }
        [Required]
        public string FileName { get; set; }

    }
}
