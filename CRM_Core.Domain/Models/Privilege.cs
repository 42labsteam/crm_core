﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class Privilege
    {
        public Guid UserId { get; set; }
        public string Entity { get; set; }
        public string Permissions { get; set; }
    }
}
