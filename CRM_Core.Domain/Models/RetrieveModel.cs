﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class RetrieveModel : InputData
    {
        public RetrieveModel(string entityId, string entityName, string operation, IEnumerable<DbModel> fields, string token) : base(entityId, entityName, operation, fields, token)
        {
        }

        public IEnumerable<DbJoinTableModel> JoinTables { get; set; }
        public IEnumerable<DbFilterModel> Filters { get; set; }
        public bool Distinct { get; set; }
        public IEnumerable<OrderBy> OrderBy { get; set; }
        public int? Page { get; set; }

    }
}
