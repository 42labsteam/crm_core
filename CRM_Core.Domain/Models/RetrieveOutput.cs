﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class RetrieveOutput : BaseOutput
    {
        public dynamic Content { get; set; }
    }
}
