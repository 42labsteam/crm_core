﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class Token
    {
        public string Id { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string Password { get; set; }
    }
}
