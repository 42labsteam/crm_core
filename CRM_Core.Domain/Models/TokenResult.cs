﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.Domain.Models
{
    public class TokenResult : BaseOutput
    {
        public string Token { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}
