﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Reposotory
{
    public interface IDbRepository
    {
        Task<bool> CreateTable(DbTableModel newEntity, string script = null);
        Task<bool> AddFK(string entityName, string tRefName, bool isAudit = false);

        //string CreateSequence (string entityName);

    }
}
