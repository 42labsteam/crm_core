﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Reposotory
{
    public interface IFileRepository
    {
        Task<BaseOutput> UploadDllFile(PluginModel input, string action, string userId);
        Task<DataTable> GetAllDllFiles();

    }
}
