﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Reposotory
{
    public interface IGenericRepository
    {
        Task<CreateOutput> Create(InputData newEntity, string userId);
        Task<GetSingleOutput> GetSingle(InputData obj, string customQuery);
        Task<bool> Delete(InputData entityId, string userId);
        Task<BaseOutput> Update(InputData updatedEntity, string userId);
        Task<RetrieveOutput> GetAll(RetrieveModel input);
        GetSingleOutput GetEntityIdFromReader(Task<DbDataReader> reader, string entityName);
        List<Dictionary<string, object>> GetAllFromReader(Task<DbDataReader> reader);
        Dictionary<string, object> GetEntityFromReader(Task<DbDataReader> reader);
        Task<DataTable> QuerySelect(string query);

    }
}
