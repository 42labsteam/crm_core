﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Reposotory
{
    public interface IUserRepository
    {
        Task<string> GetUserByLogon(string userName);
        Task<string> AuthUser(string userName, string password);
        Task<string> GetUserName(string logon);
        Task<DataTable> GetAllUsers();
    }
}
