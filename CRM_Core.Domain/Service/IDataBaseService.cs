﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Service
{
    public interface IDataBaseService
    {
        Task<BaseOutput> CreateTable(DbTableModel toCreate, string script = null);
        Task<BaseOutput> CreateAuditTable(string entityName);
        Task<RetrieveOutput> GetAllTableNames();

    }
}
