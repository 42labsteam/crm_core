﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Service
{
    public interface IFileService
    {
        Task<BaseOutput> UploadDllFile(PluginModel input, string action);
        //Task<BaseOutput> DeleteDllFile(InputData input, string userId);
        Task<List<DownloadDllOutput>> GetAllDllFiles();
        Task<List<DownloadDllOutput>> GetDllsByOperation(string operation, string entityName, int invoteAt);


    }
}
