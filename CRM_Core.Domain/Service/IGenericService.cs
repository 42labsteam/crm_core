﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Service
{
    public interface IGenericService
    {
        Task<CreateOutput> Create(InputData obj);
        Task<BaseOutput> Update(InputData obj);
        Task<BaseOutput> Delete(InputData obj);
        Task<GetSingleOutput> GetSingle(InputData obj);
        Task<RetrieveOutput> GetAll(RetrieveModel input);

    }
}
