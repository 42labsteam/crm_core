﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Service
{
    public interface IPrivilegeService
    {
        Task<CreateOutput> ValidatePrivilege(InputData input);
    }
}
