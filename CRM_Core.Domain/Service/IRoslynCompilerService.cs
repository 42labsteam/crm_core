﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Service
{
    public interface IRoslynCompilerService
    {
        Task<BaseOutput> GenerateAssembly(GenerateAssemblyModel input, string action);
        InvokeMethodOutput InvokeMethod(InvokeMethodModel input);

        BaseOutput ExecuteDlls(InputData input, IEnumerable<DownloadDllOutput> dlls);
    }
}
