﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Service
{
    public interface ITokenService
    {
        Task<TokenResult> GenerateToken(string logon);
        TokenResult CheckToken(string token);
        string GetUserFromToken(string token);
        DateTime GetTokenExpires(string token);
    }
}
