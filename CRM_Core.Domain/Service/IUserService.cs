﻿using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Domain.Service
{
    public interface IUserService
    {
        Task<string> GetGetUserByLogon (string userName);
        Task<string> GetCryptedPassword (string logon);
        string GetDecryptedPassword(string cryptoPassword);
        Task<BaseOutput> AuthUser(string userName, string password);
        Task<string> GetUserName(string logon);
        Task<BaseOutput> UpdatePasswordFromAdAuth(Auth auth);
        Task<string> GetFirstUser();
    }
}
