﻿
using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using System.Threading.Tasks;
using System.Data.Common;
using CRM_Core.SharedKernel;

namespace CRM_Core.Infra
{
    public class ConnectionFactory : IDisposable
    {
        public string ConnString { get; set; }
        private NpgsqlConnection Connection;
        private NpgsqlCommand Command;
        NpgsqlTransaction transaction = null;


        public async Task<NpgsqlConnection> GetConnection()
        {
            try
            {
                ConnString = DataBaseHelper.GetConnectionString("PostgreShiftProd");

                if (Connection == null)
                {
                    Connection = new NpgsqlConnection(ConnString);
                }

                if (Connection.State != ConnectionState.Open)
                {
                    await Connection.OpenAsync();
                    transaction = Connection.BeginTransaction();
                }

                return Connection;

            }
            catch (PostgresException ex)
            {

                if (ex.Detail != null)
                {
                    ErrorCatcher.AddError(ex.Detail);
                    return null;
                }

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetConnection", $"{ex.Message} {innerEx}");
                return null;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetConnection", $"{ex.Message} {innerEx}");
                return null;
            }

        }



        public async Task<NpgsqlCommand> GetCommand()
        {
            var conn = await GetConnection();
            return conn.CreateCommand();
        }

        public void Dispose()
        {
            try
            {
                if (Connection != null && Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }
                Connection.Dispose();
            }
            catch (PostgresException ex)
            {

                if (ex.Detail != null)
                {
                    ErrorCatcher.AddError(ex.Detail);

                }

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Dispose", $"{ex.Message} {innerEx}");
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Dispose", $"{ex.Message} {innerEx}");

            }

        }

        public async Task<DbDataReader> GetReader(string cmdText,
            CommandType cmdType = CommandType.Text,
            Dictionary<string, object> parametros = null
            )
        {
            try
            {
                using (var cmd = await GetCommand())
                {

                    cmd.CommandText = cmdText;
                    cmd.CommandType = cmdType;

                    if (parametros != null)
                        foreach (var pr in parametros)
                            cmd.Parameters.AddWithValue(pr.Key, pr.Value);

                    var result = await cmd.ExecuteReaderAsync();

                    return result;

                }
            }
            catch (PostgresException ex)
            {

                if (ex.Detail != null)
                {
                    ErrorCatcher.AddError(ex.Detail);
                    return null;
                }

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetReader", $"{ex.Message} {innerEx}");
                return null;

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetReader", $"{ex.Message} {innerEx}");
                return null;
            }


        }

        public async Task<bool> ExecuteNonQuery(string cmdText,
            CommandType cmdType = CommandType.Text,
            Dictionary<string, object> parametros = null, bool hasOtherTran = false)
        {
            try
            {
                using (var cmd = await GetCommand())
                {
                    cmd.CommandText = cmdText;
                    cmd.CommandType = cmdType;

                    if (parametros != null)
                        foreach (var pr in parametros)
                            cmd.Parameters.AddWithValue(pr.Key, pr.Value);

                    Command = cmd;

                    var result = await cmd.ExecuteNonQueryAsync();

                    if (result == 0)
                        return false;

                    if (!hasOtherTran)
                        await transaction.CommitAsync();

                    return true;
                }
            }
            catch (PostgresException ex)
            {
                try
                {

                    await transaction.RollbackAsync();

                    if (ex.Detail != null)
                    {
                        ErrorCatcher.AddError(ex.Detail);
                        return false;
                    }

                    string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                    ErrorCatcher.AddError(this.GetType().Name, "ExecuteNonQuery", $"{ex.Message} {innerEx}");
                    return false;
                }
                catch (Exception ex2)
                {
                    string innerEx = ex2.InnerException == null ? string.Empty : $" - {ex2.InnerException.Message}";
                    ErrorCatcher.AddError(this.GetType().Name, "ExecuteNonQuery", $"{ex2.Message} {innerEx}");
                    return false;
                }

            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "ExecuteNonQuery", $"{ex.Message} {innerEx}");
                return false;
            }
        }

        public async Task<object> ExecuteScalar(string cmdText,
            CommandType cmdType = CommandType.Text,
            Dictionary<string, object> parametros = null, bool hasOtherTran = false)
        {
            try
            {
                object result = null;
                using (var cmd = await GetCommand())
                {
                    cmd.CommandText = cmdText;
                    cmd.CommandType = cmdType;

                    if (parametros != null)
                        foreach (var pr in parametros)
                            cmd.Parameters.AddWithValue(pr.Key, pr.Value);

                    cmd.Parameters.Add("@retorno", NpgsqlDbType.Integer).Direction = ParameterDirection.Output;
                    Command = cmd;
                    await cmd.ExecuteScalarAsync();

                    result = cmd.Parameters["@retorno"].Value;

                    if (result == null)
                    {
                        return null;
                    }

                    if (!hasOtherTran)
                        await transaction.CommitAsync();

                    return result.ToString();


                }
            }
            catch (PostgresException ex)
            {
                try
                {

                    await transaction.RollbackAsync();

                    if (ex.Detail != null)
                    {
                        ErrorCatcher.AddError(ex.Detail);
                        return null;
                    }

                    string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                    ErrorCatcher.AddError(this.GetType().Name, "ExecuteScalar", $"{ex.Message} {innerEx}");
                    return false;
                }
                catch (Exception ex2)
                {
                    string innerEx = ex2.InnerException == null ? string.Empty : $" - {ex2.InnerException.Message}";
                    ErrorCatcher.AddError(this.GetType().Name, "ExecuteScalar", $"{ex2.Message} {innerEx}");
                    return null;
                }

            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "ExecuteScalar", $"{ex.Message} {innerEx}");
                return null;
            }
        }


    }
}
