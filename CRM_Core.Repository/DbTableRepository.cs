﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Infra;
using CRM_Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Repository
{
    public class DbTableRepository : IDbRepository
    {

        public async Task<bool> CreateTable(DbTableModel newEntity, string script = null)
        {
            string cmdText = string.Empty;

            try
            {
                using (var factory = new ConnectionFactory())
                {

                    cmdText = string.IsNullOrEmpty(script) ? DataBaseHelper.WriteCreateTableScript(newEntity) : script;

                    var result = await factory.ExecuteNonQuery(cmdText, CommandType.Text);
                    bool.TryParse(result.ToString(), out bool isSucces);

                    if (!isSucces)
                    {
                        ErrorCatcher.AddError("Error creating the new table in database");
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "CreateTable", $"{ex.Message} {innerEx}");

                return false;
            }

        }


        public async Task<bool> AddFK(string entityName, string tRefName, bool isAudit = false)
        {

            var cmdText = string.Empty;
            var wasCreated = false;


            if (isAudit)
                cmdText = $" ALTER TABLE TB_Audit_{entityName} ADD CONSTRAINT FK_{entityName}_{tRefName} FOREIGN KEY(ID_{tRefName}) REFERENCES TB_{tRefName}(ID_{tRefName}); ";
            else
                cmdText = $" ALTER TABLE TB_{entityName} ADD CONSTRAINT FK_{entityName}_{tRefName} FOREIGN KEY(ID_{tRefName}) REFERENCES TB_{tRefName}(ID_{tRefName}); ";


            try
            {
                using (var factory = new ConnectionFactory())
                {
                    wasCreated = await factory.ExecuteNonQuery(cmdText, CommandType.Text);
                }

                return wasCreated;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "AddFK", $"{ex.Message} {innerEx}");
                return false;
            }
        }



    }
}
