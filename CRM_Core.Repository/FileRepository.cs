﻿using CRM_Core.Domain.Enums;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Infra;
using CRM_Core.SharedKernel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Repository
{
    public class FileRepository : IFileRepository
    {
        private IGenericRepository _genRep;

        public FileRepository(IConfiguration config, IGenericRepository genRep)
        {
            _genRep = genRep;
        }

        public FileRepository()
        {

        }

        /// <summary>
        ///     Save the file into DataBase
        /// </summary>
        /// <param name="input"></param>
        /// <param name="assembly"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        public async Task<BaseOutput> UploadDllFile(PluginModel input, string operation, string userId)
        {
            try
            {
                if (!IsBase64(input.Base64StringDll))
                {
                    ErrorCatcher.AddError(this.GetType().Name, "UploadDllFile", "Invalid Base64String format");
                    return new BaseOutput { Success = false, ErrorMessage = "Invalid Base64String format", ErrorCode = 400 };
                }

                var parameters = new Dictionary<string, object>();
                using (var factory = new ConnectionFactory())
                {

                    //// CREATE RELATIONSHIP ////

                    // Insert into tb_dll_file
                    var fileDllFields = SetDllFileParameters(input.Base64StringDll, input.EntityName, DataBaseHelper.GetOperationIndex(input.Operation), input.InvokeAt, input.FileName);
                    var fields = DataBaseHelper.AddLogFields(fileDllFields, "create", userId);
                    parameters = DataBaseHelper.DbParametersValue(fields);
                    var cmd = DataBaseHelper.WriteInsertScript(new InputData(null, "dll_file", input.Operation, fields, null));
                    var fileId = await factory.ExecuteScalar(cmd, CommandType.Text, parameters, true);

                    if (string.IsNullOrEmpty(fileId.ToString()))
                    {
                        ErrorCatcher.AddError("Error while saving file in database");
                        return new BaseOutput { Success = false, ErrorCode = 500 };
                    }

                    // Insert into entity_dll
                    parameters = new Dictionary<string, object>();
                    var entityDllFields = SetEntityDllParameters(input.EntityName);
                    parameters = DataBaseHelper.DbParametersValue(entityDllFields);
                    var cmd2 = DataBaseHelper.WriteInsertScript(new InputData(null, "entity_dll", input.Operation, entityDllFields, null));
                    var entityId = await factory.ExecuteScalar(cmd2, CommandType.Text, parameters, true);

                    if (string.IsNullOrEmpty(entityId.ToString()))
                    {
                        ErrorCatcher.AddError("Error while saving entity_dll in database");
                        return new BaseOutput { Success = false, ErrorCode = 500 };
                    }

                    // Insert into entity_File_dll
                    parameters = new Dictionary<string, object>();
                    var entityFileFields = SetEntityFileParameters(fileId.ToString(), entityId.ToString());
                    parameters = DataBaseHelper.DbParametersValue(entityFileFields);
                    var cmd3 = DataBaseHelper.WriteInsertScript(new InputData(null, "entity_File_dll", input.Operation, entityFileFields, null));
                    var fileEntityId = await factory.ExecuteScalar(cmd3, CommandType.Text, parameters);

                    if (string.IsNullOrEmpty(fileEntityId.ToString()))
                    {
                        ErrorCatcher.AddError("Error while saving entity_File_dll in database");
                        return new BaseOutput { Success = false, ErrorCode = 500 };
                    }


                    return new BaseOutput { Success = true };

                }
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "UploadDllFile", $"{ex.Message} {innerEx}");
                return new BaseOutput { Success = false, ErrorCode = 500 };

            }
        }

        public async Task<DataTable> GetAllDllFiles()
        {
            string query = @"SELECT f.nm_file, f.ds_data, f.id_operation, e.nm_entity_name, f.id_invoke_dll FROM tb_entity_file_dll ef
                                INNER JOIN tb_dll_file f
                                ON (ef.id_dll_file = f.id_dll_file)
                                INNER JOIN tb_entity_dll e
                                ON (ef.id_entity_dll = e.id_entity_dll) 
                                WHERE f.st_delete_state = false";

            try
            {

                return await _genRep.QuerySelect(query);

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetAllDllFiles", $"{ex.Message} {innerEx}");
                return null;
            }
        }



        private List<DbModel> SetDllFileParameters(string data, string entity, int operation, int invokeAt, string dllName)
        {
            var fields = new List<DbModel>();

            fields.Add(new DbModel { ColumnName = "ds_data", Value = data });
            fields.Add(new DbModel { ColumnName = "id_operation", Value = operation });
            fields.Add(new DbModel { ColumnName = "id_invoke_dll", Value = invokeAt });
            fields.Add(new DbModel { ColumnName = "nm_file", Value = dllName });

            return fields;
        }

        private List<DbModel> SetEntityDllParameters(string entityName)
        {
            var fields = new List<DbModel>();

            fields.Add(new DbModel { ColumnName = "nm_entity_name", Value = entityName });

            return fields;
        }

        private List<DbModel> SetEntityFileParameters(string fileId, string entityId)
        {
            try
            {
                var fields = new List<DbModel>();

                fields.Add(new DbModel { ColumnName = "id_dll_file", Value = Guid.Parse(fileId) });
                fields.Add(new DbModel { ColumnName = "id_entity_dll", Value = Guid.Parse(entityId) });

                return fields;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "SetEntityFileParameters", $"{ex.Message} {innerEx}");
                return null;
            }
        }


        private bool IsBase64(string base64String)
        {
            if (string.IsNullOrEmpty(base64String) || base64String.Length % 4 != 0
               || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r") || base64String.Contains("\n"))
                return false;

            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception)
            {
                ErrorCatcher.AddError("Invalid Base64String format");
                return false;
            }
        }


    }
}
