﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Infra;
using CRM_Core.SharedKernel;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Repository
{
    public class GenericRepository : IGenericRepository
    {
        public async Task<CreateOutput> Create(InputData data, string userId)
        {

            try
            {
                using (var factory = new ConnectionFactory())
                {
                    var cmdText = string.Empty;
                    var parameters = new Dictionary<string, object>();
                    var entityName = string.Empty;
                    var fields = new List<DbModel>();

                    if (data.Operation.ToUpper().Equals("CREATE"))
                    {
                        fields = DataBaseHelper.AddLogFields(data.Fields, data.Operation, userId);
                    }

                    cmdText = DataBaseHelper.WriteInsertScript(!fields.Any() ? data : new InputData(data.EntityId, data.EntityName, data.Operation, fields, data.Token));
                    parameters = DataBaseHelper.DbParametersValue(!fields.Any() ? data.Fields : fields);

                    var entityId = await factory.ExecuteScalar(cmdText, CommandType.Text, parameters);

                    if (entityId != null)
                    {
                        object userRole = null;
                        if (data.EntityName.ToLower().Equals("user") && string.IsNullOrEmpty(data.Token))
                        {
                            userRole = await CreateUserRole(entityId.ToString());

                            if (userRole == null)
                                ErrorCatcher.AddError("The user was created sucessefuly, but has been occured an error while role relationship inserting");
                        }

                        return new CreateOutput { EntityId = entityId.ToString(), Success = true };
                    }
                    else
                    {
                        ErrorCatcher.AddError("Error creating register in database");
                        return new CreateOutput { Success = false, ErrorCode = 400 }; ;
                    }

                }

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Create", $"{ex.Message} {innerEx}");
                return new CreateOutput { Success = false, ErrorCode = 500 };
            }
        }

        public async Task<GetSingleOutput> GetSingle(InputData obj, string customQuery = "")
        {
            try
            {
                var entityFromReader = new GetSingleOutput();
                var defaultQuery = $"SELECT * FROM TB_{obj.EntityName.ToUpper()} WHERE id_{obj.EntityName.ToLower()} = '{obj.EntityId}'";
                var sqlText = string.IsNullOrEmpty(customQuery) ? defaultQuery : customQuery;

                using (var factory = new ConnectionFactory())
                {
                    var readerDaFactory = factory.GetReader(sqlText, CommandType.Text);
                    if (await readerDaFactory.Result.ReadAsync())
                    {
                        entityFromReader.Content = GetEntityFromReader(readerDaFactory);
                        entityFromReader.EntityId = GetEntityIdFromReader(readerDaFactory, obj.EntityName).EntityId;
                    }
                }
                return entityFromReader;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetSingle", $"{ex.Message} {innerEx}");
                return new GetSingleOutput { Success = false, ErrorCode = 500 };
            }
        }


        public async Task<bool> Delete(InputData obj, string userId)
        {
            string entityToDelete = GetSingle(obj).Result.EntityId;

            if (string.IsNullOrEmpty(obj.EntityId) || string.IsNullOrEmpty(obj.EntityName) || string.IsNullOrEmpty(entityToDelete))
                return false;


            try
            {
                using (var factory = new ConnectionFactory())
                {
                    var cmdText = $"UPDATE TB_{obj.EntityName} SET st_delete_state = true, dt_modifiedon = @dt_modifiedon,  nm_modifiedby = @nm_modifiedby WHERE id_{obj.EntityName} = '{obj.EntityId}'";
                    var fields = DataBaseHelper.AddLogFields(new List<DbModel>(), "UPDATE", userId);
                    var parameters = DataBaseHelper.DbParametersValue(fields);


                    var success = await factory.ExecuteNonQuery(cmdText, CommandType.Text, parameters);

                    return success;
                }
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Delete", $"{ex.Message} {innerEx}");
                return false;
            }
        }

        public async Task<BaseOutput> Update(InputData data, string userId)
        {
            var parameters = new Dictionary<string, object>();
            var cmdText = string.Empty;
            var fields = new List<DbModel>();
            try
            {
                using (var factory = new ConnectionFactory())
                {

                    if (data.Operation.ToUpper().Equals("UPDATE"))
                        fields = DataBaseHelper.AddLogFields(data.Fields, data.Operation, userId);

                    cmdText = DataBaseHelper.WriteUpdateScript(data);
                    parameters = DataBaseHelper.DbParametersValue(data.Fields);


                    var success = await factory.ExecuteNonQuery(cmdText, CommandType.Text, parameters);

                    if (!success)
                        return new RetrieveOutput { ErrorCode = 400, Success = false };

                    return new BaseOutput { Success = true };
                }
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "Update", $"{ex.Message} {innerEx}");
                return new CreateOutput { Success = false, ErrorCode = 500 };
            }

        }


        public async Task<RetrieveOutput> GetAll(RetrieveModel input)
        {
            var sb = new StringBuilder();
            var sqlText = string.Empty;
            var parameters = new Dictionary<string, object>();
            var fields = input.Fields ?? new List<DbModel>();
            input.Filters = input.Filters ?? new List<DbFilterModel>();
            input.JoinTables = input.JoinTables ?? new List<DbJoinTableModel>();
            var orderBy = input.OrderBy ?? new List<OrderBy>();


            sb.Append("SELECT ");

            if (input.Distinct)
                sb.Append("DISTINCT ");

            if (!fields.Any() && !input.JoinTables.Any())
            {
                sb.Append("* FROM TB_").Append(input.EntityName);
            }
            else
            {

                // Create Join query
                if (input.JoinTables.Any())
                {
                    sb.Append(DataBaseHelper.SetTableColumnName(fields, input.EntityName, true));
                    if (fields.Any())
                        sb.Append(",");
                    foreach (var table in input.JoinTables)
                    {
                        var joinTable = table.JoinColumns ?? new List<DbModel>();
                        if (joinTable.Any())
                        {
                            sb.Append(DataBaseHelper.SetTableColumnName(table.JoinColumns, table.JoinedTable, true));
                            sb.Append(",");
                        }

                    }
                    sqlText = sb.ToString();
                    var cols = sqlText.EndsWith(",") ? sqlText.Remove(sqlText.LastIndexOf(","), 1) : sqlText;
                    sb.Clear();
                    sb.Append(cols);
                    sb.Append(DataBaseHelper.WriteJoinScript(input));
                }
                else
                {
                    sb.Append(DataBaseHelper.SetTableColumnName(fields));
                    sb.Append(" FROM TB_").Append(input.EntityName);
                }



            }
            if (input.Filters.Any())
            {
                sb.Append(DataBaseHelper.WriteFilters(input));
                parameters = DataBaseHelper.DbFilterParameters(input);
            }


            if (orderBy.Any())
            {
                sb.Append(DataBaseHelper.WriteOrderByScript(input));
            }

            try
            {
                using (var factory = new ConnectionFactory())
                {
                    sqlText = sb.ToString();

                    var result = GetAllFromReader(factory.GetReader(sqlText, CommandType.Text, parameters));

                    if (result == null)
                    {
                        ErrorCatcher.AddError("An error occurred while retrieving the register from data base, please check the input values");
                        return new RetrieveOutput { ErrorCode = 400, Success = false };
                    }

                    return await Task.FromResult<RetrieveOutput>(new RetrieveOutput { Content = result, Success = true });
                }
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetAll", $"{ex.Message} {innerEx}");
                return new RetrieveOutput { Success = false, ErrorCode = 500 };
            }

        }


        public List<Dictionary<string, object>> GetAllFromReader(Task<DbDataReader> reader)
        {
            try
            {
                var result = new List<Dictionary<string, object>>();

                if (reader.Result == null)
                    return null;

                while (reader.Result.Read())
                {
                    var entity = GetEntityFromReader(reader);
                    if (entity != null)
                        result.Add(entity);
                }
                return result;
            }
            catch (Exception ex)
            {

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetAllFromReader", $"{ex.Message} {innerEx}");
                return null;
            }
        }


        public GetSingleOutput GetEntityIdFromReader(Task<DbDataReader> reader, string entityName)
        {
            try
            {
                var entityId = reader.Result[$"id_{entityName}"].ToString();

                return new GetSingleOutput
                {
                    EntityId = entityId
                };
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetEntityIdFromReader", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public Dictionary<string, object> GetEntityFromReader(Task<DbDataReader> reader)
        {
            try
            {
                var dbValues = DataBaseHelper.GetGenericDbValues(reader);
                return dbValues;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetEntityFromReader", $"{ex.Message} {innerEx}");
                return null;
            }

        }

        public async Task<DataTable> QuerySelect(string query)
        {
            try
            {
                DataTable table = new DataTable();

                using (var factory = new ConnectionFactory())
                {
                    NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(query, await factory.GetConnection());
                    adapter.Fill(table);
                }

                return table;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "QuerySelect", $"{ex.Message} {innerEx}");
                return null;
            }
        }
        private async Task<BaseOutput> CreateUserRole(string userId)
        {
            var inputData = new InputData(null, "user_role", "create", new List<DbModel>
                         {
                             new DbModel { ColumnName = "id_role", Value = "de33a3aa-d815-4fa0-89cf-e0f65bddfed9" },
                             new DbModel { ColumnName = "id_user", Value = userId }
                         }, null);

            var result = await Create(inputData, userId);
            return result;
        }
    }
}
