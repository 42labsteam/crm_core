﻿using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Infra;
using CRM_Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IGenericRepository _genRep;

        public UserRepository(IGenericRepository genRep)
        {
            _genRep = genRep;
        }

        public UserRepository() { }

        public async Task<string> GetUserByLogon(string userName)
        {
            var cmdText = $"SELECT id_user FROM TB_USER WHERE nm_logon = '{userName}' AND st_delete_state = false";
            object userId = null;

            try
            {
                using (var factory = new ConnectionFactory())
                {
                    userId = await factory.ExecuteScalar(cmdText, CommandType.Text);
                }

                if (userId == null)
                {
                    return null;
                }


                return userId.ToString();
            }
            catch (Exception ex)
            {

                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetUserByLogon", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public async Task<string> AuthUser(string userName, string password)
        {
            var cmdText = $"SELECT id_user FROM TB_USER WHERE nm_logon = '{userName}' AND ds_password = '{password}' AND st_delete_state = false";
            object userId = null;

            try
            {
                using (var factory = new ConnectionFactory())
                {
                    userId = await factory.ExecuteScalar(cmdText, CommandType.Text);
                }

                if (userId == null)
                    return null;

                return userId.ToString();
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "AuthUser", $"{ex.Message} {innerEx}");
                return null;
            }
        }

        public async Task<string> GetUserName(string logon)
        {
            var cmdText = $"SELECT nm_full_name FROM TB_USER WHERE nm_logon = '{logon}'  AND st_delete_state = false";
            object userId = null;

            try
            {
                using (var factory = new ConnectionFactory())
                {
                    userId = await factory.ExecuteScalar(cmdText, CommandType.Text);
                }

                if (userId == null)
                    return null;

                return userId.ToString();
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetUserName", $"{ex.Message} {innerEx}");
                return null;
            }

        }

        public async Task<DataTable> GetAllUsers()
        {
            var cmdText = $"SELECT * FROM TB_USER WHERE st_delete_state = false";

            try
            {
                var table = await _genRep.QuerySelect(cmdText);

                return table;
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError(this.GetType().Name, "GetAllUsers", $"{ex.Message} {innerEx}");
                return null;
            }
        }
    }
}
