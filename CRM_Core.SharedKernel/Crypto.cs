﻿using CRM_Core.Domain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CRM_Core.SharedKernel
{
    public static class Crypto
    {
        public static string EncryptString(string Message)
        {
            string _results = string.Empty;

            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes("NetCore_:_CoreCRM"));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(Message);

            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            _results = Convert.ToBase64String(Results);
            return _results;
        }

        public static string DecryptString(string Message)
        {
            try
            {
                byte[] Results;
                System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
                MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
                byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes("NetCore_:_CoreCRM"));
                TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
                TDESAlgorithm.Key = TDESKey;
                TDESAlgorithm.Mode = CipherMode.ECB;
                TDESAlgorithm.Padding = PaddingMode.PKCS7;
                byte[] DataToDecrypt = Convert.FromBase64String(Message);
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);

                return UTF8.GetString(Results);
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError("Crypto", "DecryptString", $"{ex.Message} {innerEx}");

                return null;
            }


        }

        public static BaseOutput CryptPassword(InputData input)
        {

            try
            {
                string password = string.Empty;
                string cryptedPwd = string.Empty;
                int pwdIndex = -1;

                var updateInput = new InputData(input.EntityId, input.EntityName, input.Operation ?? "create", input.Fields, input.Token);
                if (!ObjectValidator.IsValidInputData(updateInput))
                {

                    return new BaseOutput
                    {
                        Success = false,
                       ErrorCode = 400
                    };
                }

                if (input.Fields != null)
                {
                    pwdIndex = input.Fields.ToList().FindLastIndex(x => x.ColumnName.ToLower().Equals("ds_password"));

                    if (pwdIndex > -1)
                    {
                        password = input.Fields.FirstOrDefault(x => x.ColumnName.ToLower().Equals("ds_password")).Value.ToString();

                        if (string.IsNullOrEmpty(password) && input.Operation.ToLower().Equals("create"))
                            return new BaseOutput { ErrorMessage = "Empty password", ErrorCode = 400, Success = false };

                        if (!string.IsNullOrEmpty(password))
                        {
                            cryptedPwd = Crypto.EncryptString(password);

                            if (!string.IsNullOrEmpty(cryptedPwd))
                                input.Fields.ToList()[pwdIndex].Value = cryptedPwd;
                        }

                    }
                }

                return new BaseOutput { Success = true };

            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                ErrorCatcher.AddError("Crypto", "CryptPassword", $"{ex.Message} {innerEx}");
                return new BaseOutput { ErrorCode = 500, Success = false };
            }
        }
    }
}
