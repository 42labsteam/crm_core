﻿using CRM_Core.Domain.Enums;
using CRM_Core.Domain.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.SharedKernel
{
    public static class DataBaseHelper
    {

        public static IConfigurationRoot ConfigRoot { get; set; }

        /// <summary>
        ///     Create and Update the log of modified fields
        /// </summary>
        /// <param name="inputFields">Can have some values. Is going to be add the logs fields</param>
        /// <param name="action">CREATE OR UPTADE</param>
        /// <param name="user">User who have modified or created any register in database</param>
        /// <returns>Returns a list of fields that will be modified at each request</returns>
        public static List<DbModel> AddLogFields(IEnumerable<DbModel> inputFields, string action, string user)
        {
            List<DbModel> fields = inputFields.ToList();
            var fmtDateNow = DbDateFormat(DateTime.Now);

            switch (action.ToUpper())
            {
                case "CREATE":
                    fields.Add(new DbModel { ColumnName = "dt_createdon", Value = fmtDateNow });
                    fields.Add(new DbModel { ColumnName = "nm_createdby", Value = Guid.Parse(user) });
                    fields.Add(new DbModel { ColumnName = "dt_modifiedon", Value = fmtDateNow });
                    fields.Add(new DbModel { ColumnName = "nm_modifiedby", Value = Guid.Parse(user) });
                    break;
                case "UPDATE":
                    fields.Add(new DbModel { ColumnName = "dt_modifiedon", Value = fmtDateNow });
                    fields.Add(new DbModel { ColumnName = "nm_modifiedby", Value = Guid.Parse(user) });
                    break;
            }

            return fields;
        }

        public static string WriteCreateTableScript(DbTableModel newEntity)
        {
            string cmdText = string.Empty;

            var sb = new StringBuilder();
            sb.Append("CREATE TABLE TB_").Append(newEntity.EntityName);
            sb.Append("(");
            sb.Append($"ID_{newEntity.EntityName} UUID PRIMARY KEY DEFAULT gen_random_uuid(),");

            var fieldCount = 0;

            foreach (var field in newEntity.Fields)
            {
                fieldCount++;
                sb.Append(fieldCount == newEntity.Fields.Count() ? $"{field.ColumnName} {field.Value} {IsRequired(field.isRequired)} " : $"{field.ColumnName} {field.Value} {IsRequired(field.isRequired)}, ");
            }

            sb.Append(");");

            cmdText = sb.ToString();

            return cmdText;
        }

        /// <summary>
        ///  Create a generic SQL insert line
        /// </summary>
        /// <param name="data">Contains the columm names</param>
        /// <returns>Returns de script</returns>
        public static string WriteInsertScript(InputData data)
        {
            var cmdText = string.Empty;
            var sb = new StringBuilder();

            if (data.Operation.ToUpper().Equals("AUDIT"))
            {
                sb.Append("INSERT INTO TB_AUDIT_").Append(data.EntityName);
            }
            else
            {
                sb.Append("INSERT INTO TB_").Append(data.EntityName);
            }
            sb.Append(" (");

            sb.Append(SetTableColumnName(data.Fields));

            sb.Append(")");
            sb.Append($" VALUES (");

            sb.Append(SetTableColumnWithParameters(data.Fields));


            sb.Append(")");

            if (data.Operation.ToUpper().Equals("AUDIT"))
            {
                sb.Append($"returning id_audit_{ data.EntityName.ToLower()}");
            }
            else
            {
                sb.Append($"returning id_{ data.EntityName.ToLower()}");
            }


            cmdText = sb.ToString();

            return cmdText;
        }

        public static string WriteOrderByScript(RetrieveModel input)
        {

            var sb = new StringBuilder();
            var auxCount = 0;
            var auxTable = string.Empty;

            sb.Append(" ORDER BY ");

            foreach (var item in input.OrderBy)
            {
                auxCount++;

                if (!string.IsNullOrEmpty(item.TableName))
                    sb.Append($"t_{item.TableName.ToLower()}.");

                sb.Append(item.ColumnName);

                switch (item.Direction)
                {
                    case OrderByDirection.ASC:
                        sb.Append(" ASC");
                        break;
                    case OrderByDirection.DESC:
                        sb.Append(" DESC");
                        break;
                    default:
                        break;
                }

                sb.Append(auxCount < input.OrderBy.Count() ? ", " : string.Empty);
            }

            return sb.ToString();
        }

        /// <summary>
        ///  Create the parameters that will be related to the scripts input values
        /// </summary>
        /// <param name="fields">Collum name and the input value</param>
        /// <returns></returns>
        public static Dictionary<string, object> DbParametersValue(IEnumerable<DbModel> fields)
        {
            var parameters = new Dictionary<string, object>();

            foreach (var field in fields)
            {
                field.Value = field.Value ?? DBNull.Value;
                if (DateTime.TryParse(field.Value.ToString(), out DateTime date) && !Double.TryParse(field.Value.ToString(), out double db))
                {
                    parameters.Add($"@{field.ColumnName}", date);
                }
                else if (Guid.TryParse(field.Value.ToString(), out Guid id))
                {
                    parameters.Add($"@{field.ColumnName}", id);
                }
                else
                {
                    parameters.Add($"@{field.ColumnName}", field.Value);
                }



                if (field.Value == null)
                    parameters.Remove($"@{field.ColumnName}");
            }

            return parameters;


        }

        /// <summary>
        ///  Create a generic SQL update line
        /// </summary>
        /// <param name="data">Contains the columm names</param>
        /// <returns>Returns de script</returns>
        public static string WriteUpdateScript(InputData data)
        {
            var cmdText = string.Empty;
            var sb = new StringBuilder();
            var fieldCount = 0;

            sb.Append("UPDATE TB_").Append(data.EntityName);
            sb.Append(" SET ");

            foreach (var field in data.Fields)
            {
                fieldCount++;
                sb.Append(fieldCount == data.Fields.Count() ? $"{field.ColumnName} = @{field.ColumnName}" : $"{field.ColumnName} = @{field.ColumnName} , ");

            }

            sb.Append($" WHERE id_{data.EntityName} = '{data.EntityId}'");

            cmdText = sb.ToString();

            return cmdText;
        }




        public static string WriteFilters(RetrieveModel input)
        {
            var sb = new StringBuilder();
            var result = string.Empty;
            var auxCount = 0;
            var joinTable = string.Empty;

            try
            {
                sb.Append(" WHERE ");
                foreach (var filter in input.Filters)
                {
                    auxCount++;

                    if (input.JoinTables.Any() && !string.IsNullOrEmpty(filter.EntityName))
                        joinTable = $"t_{filter.EntityName}.";

                    if (filter.LogicalOperator == LogicalOperators.NOT)
                    {
                        sb.Append("NOT ").Append(joinTable).Append(filter.ColumnName).Append(filter.Comparison > 0 ? $" {SetComparisonOperator(filter.Comparison)} " : " ").Append($"@{filter.ColumnName}");
                    }
                    else
                    {

                        switch (filter.LogicalOperator)
                        {
                            case LogicalOperators.BETWEEN:
                                sb.Append(joinTable).Append(filter.ColumnName).Append($" BETWEEN @BetweenValue ").Append(" AND ").Append("@BetweenValue2");
                                break;
                            case LogicalOperators.LIKE:
                                sb.Append(joinTable).Append(filter.ColumnName).Append(" ILIKE '%").Append($"{filter.Value}%'");
                                break;
                            case LogicalOperators.IS_NULL:
                                sb.Append(joinTable).Append(filter.ColumnName).Append(" IS NULL ");
                                break;
                            case 0:
                                sb.Append(joinTable).Append(filter.ColumnName).Append($" {SetComparisonOperator(filter.Comparison)} ").Append($"@{filter.ColumnName}");
                                break;
                        }

                    }


                    sb.Append(!string.IsNullOrEmpty(SetConcatOperator(filter.ConcatOperator)) && auxCount < input.Filters.Count()
                        ? $" {SetConcatOperator(filter.ConcatOperator)} " : string.Empty);


                }

                return sb.ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static Dictionary<string, object> DbFilterParameters(RetrieveModel input)
        {
            var parameters = new Dictionary<string, object>();
            parameters = DbParametersValue(input.Filters);

            foreach (var filter in input.Filters)
            {

                if (filter.LogicalOperator == LogicalOperators.BETWEEN)
                {
                    if (DateTime.TryParse(filter.BetweenValue.ToString(), out DateTime value))
                    {
                        parameters.Add("@BetweenValue", value);
                    }
                    else
                    {
                        parameters.Add("@BetweenValue", filter.BetweenValue);
                    }

                    if (DateTime.TryParse(filter.BetweenValue2.ToString(), out DateTime value2))
                    {
                        parameters.Add("@BetweenValue2", value2);
                    }
                    else
                    {
                        parameters.Add("@BetweenValue2", filter.BetweenValue2);

                    }




                }
            }

            return parameters;


        }

        /// <summary>
        ///     Format a DateTime
        /// </summary>
        /// <param name="date">Any DateTime</param>
        /// <returns>Formated DateTime</returns>
        public static DateTime DbDateFormat(DateTime date)
        {
            string fmtDate = date.ToString("yyyy-MM-dd HH:mm:ss");

            return StringToDate(fmtDate);
        }

        private static DateTime StringToDate(string strdate)
        {
            DateTime dbDate = DateTime.ParseExact(strdate, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

            return dbDate;
        }

        /// <summary>
        ///     Get a any table from database and map the columm and the collums value
        /// </summary>
        /// <param name="reader">Table values</param>
        /// <returns>Return table columm names and their values</returns>
        public static Dictionary<string, object> GetGenericDbValues(Task<DbDataReader> reader)
        {
            var genericObj = new Dictionary<string, object>();

            for (int col = 0; col < reader.Result.FieldCount; col++)
            {
                var colName = reader.Result.GetName(col).ToString();
                var colValue = reader.Result[colName].ToString();


                genericObj.Add(colName, colValue);

            }

            return genericObj;
        }

        public static string SetTableColumnName(IEnumerable<DbModel> columns, string entityname = null, bool hasJoin = false)
        {
            var sb = new StringBuilder();
            var fieldCount = 0;
            var alias = hasJoin ? $"t_{entityname}." : string.Empty;


            foreach (var column in columns)
            {
                fieldCount++;

                if (column.ColumnName.ToLower().Equals("start"))
                {
                    column.ColumnName = "dt_start";
                }

                if (column.ColumnName.ToLower().Equals("end"))
                {
                    column.ColumnName = "dt_end";
                }

                if (column.ColumnName.ToLower().Equals("distinct") && fieldCount == 1)
                    sb.Append(column.ColumnName).Append(" ");
                else
                    sb.Append(fieldCount == columns.Count() ? $"{alias}{column.ColumnName}" : $"{alias}{column.ColumnName},");
            }

            return sb.ToString();
        }

        public static string SetTableColumnWithParameters(IEnumerable<DbModel> columns)
        {
            var sb = new StringBuilder();
            var fieldCount = 0;


            foreach (var field in columns)
            {
                fieldCount++;

                sb.Append(fieldCount == columns.Count() ? $"@{field.ColumnName}" : $"@{field.ColumnName},");


            }

            return sb.ToString();
        }

        private static string IsRequired(bool value)
        {
            switch (value)
            {
                case true:
                    return "NOT NULL";
                case false:
                    return "NULL";
                default:
                    return "NULL";

            }
        }

        public static int GetOperationIndex(string operation)
        {
            List<String> operations = new List<string>();
            operations.AddRange(new string[] { "create", "update", "retrieve", "delete" });

            if (operation.ToLower().Contains("retrieve"))
            {
                operation = "retrieve";
            }

            return operations.IndexOf(operation.ToLower());

        }


        public static string WriteJoinScript(RetrieveModel input)
        {

            var sb = new StringBuilder();
            var auxCount = 0;
            var auxTable = string.Empty;

            sb.Append(" FROM TB_").Append(input.EntityName).Append(" AS t_").Append(input.EntityName);
            auxTable = input.EntityName;

            foreach (var table in input.JoinTables)
            {
                auxCount++;
                switch (table.JoinType)
                {
                    case JoinTypes.LEFT_JOIN:
                        sb.Append(" LEFT JOIN TB_").Append(table.JoinedTable).Append(" AS t_").Append(table.JoinedTable);
                        break;
                    case JoinTypes.RIGHT_JOIN:
                        sb.Append(" RIGHT JOIN TB_").Append(table.JoinedTable).Append(" AS t_").Append(table.JoinedTable);
                        break;
                    case JoinTypes.INNER_JOIN:
                        sb.Append(" INNER JOIN TB_").Append(table.JoinedTable).Append(" AS t_").Append(table.JoinedTable);
                        break;
                    case JoinTypes.FULL_OUTER_JOIN:
                        sb.Append(" FULL OUTTER JOIN TB_").Append(table.JoinedTable).Append(" AS t_").Append(table.JoinedTable);
                        break;
                    default:
                        sb.Append(" INNER JOIN TB_").Append(table.JoinedTable).Append(" AS t_").Append(table.JoinedTable);
                        break;
                }

                sb.Append(" ON (t_").Append(table.LeftJoinTable).Append(".").Append("id_").Append(table.LeftJoinId);
                sb.Append(" = t_").Append(table.RightJoinTable).Append(".").Append("id_").Append(table.RightJoinId).Append(")");

                auxTable = table.JoinedTable;
            }

            return sb.ToString();

        }


        private static string SetComparisonOperator(ComparisonOperators op)
        {
            string result = string.Empty;

            if (Enum.IsDefined(typeof(ComparisonOperators), op))
            {
                switch (op)
                {
                    case ComparisonOperators.EQUAL_TO:
                        result = "=";
                        break;
                    case ComparisonOperators.GREATHER_THAN:
                        result = ">";
                        break;
                    case ComparisonOperators.LESS_THAN:
                        result = "<";
                        break;
                    case ComparisonOperators.GREATHER_THAN_OR_EQUAL_TO:
                        result = ">=";
                        break;
                    case ComparisonOperators.LESS_THAN_OR_EQUAL_TO:
                        result = "<=";
                        break;
                    case ComparisonOperators.NOT_EQUAL_TO:
                        result = "<>";
                        break;
                }
            }
            else
            {
                throw new Exception("Invalid Comparison Operator");
            }


            return result;

        }



        private static string SetConcatOperator(LogicalOperators op)
        {
            string result = string.Empty;

            if (Enum.IsDefined(typeof(LogicalOperators), op))
            {
                switch (op)
                {
                    case LogicalOperators.AND:
                        result = op.ToString();
                        break;
                    case LogicalOperators.OR:
                        result = op.ToString();
                        break;
                }
            }

            return result;

        }

        public static string GetConnectionString(string connString)
        {
            try
            {
                var result = ConfigRoot[$"ConnectionStrings:{connString}"];

                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public static string FormmatToDbEntityName(string name)
        {
            var fmtName = name;

            if (name.Contains(' '))
                fmtName = name.Replace(' ', '_');

            return fmtName.Trim().ToLower();
        }

    }
}
