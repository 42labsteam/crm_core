﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM_Core.SharedKernel
{
    public static class ErrorCatcher
    {
        public static List<string> ErrorList = new List<string>();

        public static void AddError(string className, string methodName, string errorMessage)
        {
            ErrorList.Add($"ClassName: {className} > ClassMethod: {methodName} > ErrorMessage: {errorMessage}");
        }

        public static void AddError(string errorMessage)
        {
            ErrorList.Add(errorMessage);
        }

        public static string SetErrors()
        {
            try
            {
                string error = string.Empty;

                if (ErrorList.Count == 0)
                    return string.Empty;
                else if (ErrorList.Count == 1)
                {
                    return JsonConvert.SerializeObject(ErrorList);
                }
                else if (ErrorList.Count > 1)
                {
                    var resultList = new List<string>();
                    int errorCount = 1;
                    foreach (var e in ErrorList)
                    {
                        resultList.Add($"{errorCount} - {e}");
                        errorCount++;
                    }
                    return JsonConvert.SerializeObject(resultList);
                }
                else
                    return string.Empty;

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public static void ClearErrors()
        {
            ErrorList = new List<string>();
        }
    }
}
