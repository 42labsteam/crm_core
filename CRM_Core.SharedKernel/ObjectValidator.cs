﻿using CRM_Core.Domain.Enums;
using CRM_Core.Domain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRM_Core.SharedKernel
{
    public static class ObjectValidator
    {
        public static bool IsValidTabelModel(DbTableModel obj)
        {

            int errorCount = 0;

            if (obj == null)
            {
                errorCount++;
                ErrorCatcher.AddError($"The object is null");
            }
            else
            {
                if (string.IsNullOrEmpty(obj.EntityName))
                {
                    errorCount++;
                    ErrorCatcher.AddError($"Please, enter the name");

                }

                if (obj.Fields.Count() < 1)
                {
                    errorCount++;
                    ErrorCatcher.AddError($"Please, enter at least one field");
                }
            }

            if (errorCount > 0)
                return false;
            else
                return true;

        }

        public static bool IsValidInputBaseInfo(InputData obj)
        {

            string operation = obj.Operation.ToUpper();
            int errorCount = 0;


            if (obj == null)
            {
                errorCount++;
                ErrorCatcher.AddError($"The object is null ");
            }
            else
            {
                if (string.IsNullOrEmpty(obj.EntityName))
                {
                    errorCount++;
                    ErrorCatcher.AddError("Please, enter the entity name");
                }
            }

            if (errorCount > 0)
                return false;
            else
                return true;
        }

        public static bool IsValidInputData(InputData obj)
        {

            string operation = obj.Operation.ToUpper();
            int errorCount = 0;


            if (obj == null)
            {
                errorCount++;
                ErrorCatcher.AddError($"The object is null ");
            }
            else
            {
                if (string.IsNullOrEmpty(obj.EntityName))
                {
                    errorCount++;
                    ErrorCatcher.AddError("Please, enter the entity name");
                }

                if (operation.Equals("CREATE") || operation.Equals("UPDATE"))
                {
                    if (!obj.Fields.Any())
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"Please, enter at least one field");
                    }
                }

                if (string.IsNullOrEmpty(obj.EntityId) && (operation.Equals("RETRIEVE") || operation.Equals("UPDATE") || operation.Equals("DELETE")))
                {
                    errorCount++;
                    ErrorCatcher.AddError($"Please, enter the EntityId");
                }
            }

            if (errorCount > 0)
                return false;
            else
                return true;
        }


        public static bool IsValidAuthObject(Auth obj)
        {
            int errorCount = 0;


            if (obj == null)
            {
                errorCount++;
                ErrorCatcher.AddError($"The object is null");
            }
            else
            {
                if (string.IsNullOrEmpty(obj.User))
                {
                    errorCount++;
                    ErrorCatcher.AddError($"Please, enter the username");
                }


                if (string.IsNullOrEmpty(obj.Password))
                {
                    errorCount++;
                    ErrorCatcher.AddError($"Please, enter the password ");
                }
            }

            if (errorCount > 0)
                return false;
            else
                return true;
        }


        public static bool IsValidDbFilterObject(RetrieveModel obj)
        {
            int errorCount = 0;
            int filterIndex = 0;
            int countJoinTables = obj.JoinTables.Count();
            int countFilterTables = obj.Filters.Count();

            if (obj.Filters.Any())
            {
                foreach (var filter in obj.Filters)
                {
                    filterIndex++;
                    if (string.IsNullOrEmpty(filter.ColumnName))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"Please, enter the column name for the filter in index {filterIndex} ");
                    }

                    if (filter.Value == null)
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"Please, enter the value for the filter in index {filterIndex} ");
                    }

                    if (filter.Comparison == 0 && filter.LogicalOperator == 0)
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - Please, enter a 'Logical Operator' or 'Comparison Operator' for the filter in index {filterIndex}");
                    }

                    if (countJoinTables > 0 && string.IsNullOrEmpty(filter.EntityName))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - Please, enter the 'Entity Name' to specify which table will be related to the filter in index {filterIndex}  ");
                    }

                    if ((countFilterTables > 1) &&
                        (filterIndex < countFilterTables) &&
                        (filter.ConcatOperator == 0) &&
                        (filter.ConcatOperator != LogicalOperators.AND && filter.ConcatOperator != LogicalOperators.OR))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - Please, enter the 'Concat Operator' (AND | OR) for the filter in index {filterIndex} ");
                    }
                }
            }

            if (errorCount > 0)
                return false;
            else
                return true;
        }



        public static bool IsValidDbJoinTableModel(RetrieveModel obj)
        {
            int errorCount = 0;

            if (obj.JoinTables.Any())
            {
                foreach (var table in obj.JoinTables)
                {
                    if (string.IsNullOrEmpty(table.JoinedTable))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - Please, enter the table name that you want to join ");
                    }
                    if (string.IsNullOrEmpty(table.LeftJoinTable))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - [JoinedTable: {table.JoinedTable}] Please, enter the left table name ");

                    }

                    if (string.IsNullOrEmpty(table.LeftJoinId))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - [JoinedTable: {table.JoinedTable}] Please, enter the FK of the left table ");
                    }

                    if (string.IsNullOrEmpty(table.RightJoinTable))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - [JoinedTable: {table.JoinedTable}] Please, enter the right table name ");
                    }

                    if (string.IsNullOrEmpty(table.RightJoinId))
                    {
                        errorCount++;
                        ErrorCatcher.AddError($"{errorCount} - [JoinedTable: {table.JoinedTable}] Please, enter the PK of the right table ");
                    }
                }

            }

            if (errorCount > 0)
                return false;
            else
                return true;
        }
    }
}
