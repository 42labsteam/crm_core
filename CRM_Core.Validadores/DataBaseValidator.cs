﻿using CRM_Core.Application;
using CRM_Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM_Core.Validadores
{
    public static class DataBaseValidator
    {
        public static async Task<BaseOutput> CheckCnpj(InputData input)
        {
            try
            {
                GenericService svc = new GenericService();
                var field = input.Fields.Where(f => f.ColumnName.Equals("nr_cnpj")).FirstOrDefault() ?? new DbModel();

                if (field == null)
                    new BaseOutput { Success = false, ErrorCode = 500, ErrorMessage = "Can't find cnpj field" };


                var cnpj = field.Value.ToString();

                var retrieveModel = new RetrieveModel
                {
                    EntityName = input.EntityName,
                    Fields = new List<DbModel> { new DbModel { ColumnName = "nr_cnpj" } },
                    Filters = new List<DbFilterModel> { new DbFilterModel { ColumnName = "nr_cnpj", Value = cnpj } }

                };

                var result = await svc.GetAll(retrieveModel);

                if (result.Success && !string.IsNullOrEmpty(result.Content))
                    return new BaseOutput { Success = false, ErrorCode = 400, ErrorMessage = "CNPJ already exists" };

                return new BaseOutput { Success = true };
            }
            catch (Exception ex)
            {

                return new BaseOutput { Success = false, ErrorCode = 500, ErrorMessage = ex.Message };
            }




        }
    }
}
