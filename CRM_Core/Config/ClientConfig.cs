﻿using CRM_Core.SharedKernel;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CRM_Core.Config
{
    public static class ClientConfig
    {
        public static void Configure(this IServiceCollection services)
        {
            Uri baseUrl = new Uri(DataBaseHelper.GetConnectionString("Webtask"));
            HttpClientHandler handler = new HttpClientHandler();
            HttpClient httpClient = new HttpClient(handler, false)
            {
                BaseAddress = baseUrl,
            };
            services.AddSingleton<HttpClient>(httpClient);
        }
    }
}
