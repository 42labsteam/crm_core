﻿using CRM_Core.Application;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Reposotory;
using CRM_Core.Domain.Service;
using CRM_Core.Filters;
using CRM_Core.Infra;
using CRM_Core.Repository;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM_Core.Config
{
    public static class ServicesConfig
    {
        public static void Configure(this IServiceCollection services)
        {
            #region ApplicationService
            services.AddTransient<IDataBaseService, DataBaseService>();
            services.AddTransient<IGenericService, GenericService>();
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IPrivilegeService, PrivilegeService>();
            services.AddTransient<ITokenService, TokenService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAuditService, AuditService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IRoslynCompilerService, RoslynCore.RoslynCompiler>();
            #endregion

            #region Repository
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IGenericRepository, GenericRepository>();
            services.AddTransient<IFileRepository, FileRepository>();
            #endregion

            #region HttpContext
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IHostedService, FileService>();
            #endregion

            #region Filters
            services.AddScoped<CustomActionFilter>();
            #endregion

            #region MemoryCache
            services.AddMemoryCache();
            #endregion


        }
    }
}
