﻿using CRM_Core.Application;
using CRM_Core.Domain.Enums;
using CRM_Core.Domain.Models;
using CRM_Core.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CRM_Core.Config
{
    public static class StartupMethods
    {
        private static readonly GenericService genSvc = new GenericService();
        private static readonly GenericRepository genRep = new GenericRepository();
        private static readonly UserService userSvc = new UserService();
        private static readonly DataBaseService dbSvc = new DataBaseService();
        private static string userId = string.Empty;
        private static string ROLE_ID = "76caa61e-2663-4e34-afe2-2eefebf895c0"; // ID of the super role
        private static bool hasRole = false;

        public static void ExecuteMethods()
        {
            try
            {
                userId = GetFirstUser().Result;

                if (!string.IsNullOrEmpty(userId))
                    CreateInicialPrivilege();
            }
            catch (Exception ex)
            {

                new Exception(ex.Message);
            }
        }

        private static void CreateInicialPrivilege()
        {
            try
            {
                List<DbModel> values = new List<DbModel>();
                List<DbModel> previousPermissions = new List<DbModel>();
                List<string> tables = new List<string>();


                CreateInicialUserRole();

                if (hasRole)
                {
                    var output = GetAllTablesName();
                    var json = JsonConvert.SerializeObject(output);
                    tables = JsonConvert.DeserializeObject<List<string>>(json);

                    previousPermissions = GetPreviousPermissions();

                    foreach (var table in tables)
                    {
                        if (!previousPermissions.Exists(x => x.ColumnName == ROLE_ID && x.Value.ToString() == table))
                        {
                            values.Add(new DbModel { ColumnName = "id_role", Value = ROLE_ID });
                            values.Add(new DbModel { ColumnName = "id_entity", Value = table });
                            values.Add(new DbModel { ColumnName = "permissions", Value = "1111" });
                            var inputData = new InputData(null, "privilege", "create", values, null);
                            genRep.Create(inputData, userId).ConfigureAwait(false);
                            values = new List<DbModel>();
                        }

                    }
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        private static void CreateInicialUserRole()
        {
            var role = genRep.GetSingle(new InputData(ROLE_ID, "role", null, null, null)).Result;


            if (string.IsNullOrEmpty(role.EntityId))
                return;
            var retriveModel = new RetrieveModel(null, "user_role", null, null, null);
            retriveModel.Filters = new List<DbFilterModel> {
                        new DbFilterModel {
                            ColumnName = "id_role",
                            Value = ROLE_ID,
                            Comparison = ComparisonOperators.EQUAL_TO,
                            ConcatOperator = LogicalOperators.AND
                        },
                        new DbFilterModel {
                            ColumnName = "id_user",
                            Value = userId,
                            Comparison = ComparisonOperators.EQUAL_TO
                        }
                    };

            hasRole = true;

            var user_role = genRep.GetAll(retriveModel).Result;
            var json = JsonConvert.SerializeObject(user_role.Content);
            List<Dictionary<string, object>> content = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json);

            if (user_role.Content == null || !content.Any())
            {
                var inputData = new InputData(null, "user_role", "create", new List<DbModel>
                         {
                             new DbModel { ColumnName = "id_role", Value = ROLE_ID },
                             new DbModel { ColumnName = "id_user", Value = userId }
                         }, null);

                genRep.Create(inputData, userId).ConfigureAwait(false);
            }

        }

        public async static Task<string> GetFirstUser()
        {
            var cmdText = $"SELECT * FROM TB_USER WHERE st_delete_state = false";
            string userId = string.Empty;
            List<DbModel> users = new List<DbModel>();
            string result = string.Empty;

            var table = await genRep.QuerySelect(cmdText);

            foreach (DataRow row in table.Rows)
            {
                users.Add(new DbModel { ColumnName = row["id_user"].ToString(), Value = row["dt_createdon"].ToString() });
            }

            if (users.Any())
            {
                var orderList = users.OrderBy(x => x.Value);
                var firstValue = orderList.FirstOrDefault();
                result = firstValue.ColumnName;
            }

            return result;
        }

        private static List<string> GetAllTablesName()
        {
            var tableNames = new List<string>();
            string tableName = string.Empty;
            string fmtTableName = string.Empty;

            var sqlQuery = @"
                             select * 
                            from tb_entity 
                            where nm_db_name not like '%audit%' 
                            and nm_db_name not like '%log%' 
                            and nm_db_name not like '%entity_dll%'
                            and nm_db_name not like '%invoke_dll%'
                            and nm_pt_label not like '%audit%' 
                            and nm_pt_label not like '%log%';";

            var table = genRep.QuerySelect(sqlQuery).Result;

            foreach (DataRow row in table.Rows)
            {
                tableName = row["id_entity"].ToString().ToLower();
                tableNames.Add(tableName);
            }

            return tableNames;
        }

        private static List<DbModel> GetPreviousPermissions()
        {
            var cmdText = @"SELECT * FROM TB_PRIVILEGE WHERE id_entity IS NOT NULL";
            List<DbModel> privileges = new List<DbModel>();

            var table = genRep.QuerySelect(cmdText).Result;

            foreach (DataRow row in table.Rows)
            {
                privileges.Add(new DbModel { ColumnName = row["id_role"].ToString(), Value = row["id_entity"].ToString() });
            }


            return privileges;
        }


    }

}
