﻿using CRM_Core.SharedKernel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CRM_Core.Config
{
    public static class WebHostConfig
    {
        public static void SetKestrelOptions(this KestrelServerOptions options)
        {
            var certificate = DataBaseHelper.GetConnectionString("Certificate");
            var certificatePassword = DataBaseHelper.GetConnectionString("CertificatePassword");

            options.Listen(IPAddress.Any, 80);
            options.Listen(IPAddress.Any, 443, listenOptions =>
            {
                listenOptions.UseHttps(certificate, certificatePassword);

            });
        }
    }
}
