﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using CRM_Core.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CRM_Core.Controllers
{
    [Route("api/v1/[controller]")]
    [ServiceFilter(typeof(CustomActionFilter))]
    public class AuthController : Controller
    {

        private IHttpContextAccessor _accessor;

        public AuthController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        [HttpPost]
        public async Task<IActionResult> Auth([FromBody] Auth auth, [FromServices] IAuthService adSvc,
            [FromServices] ITokenService tokenSvc, [FromServices] IAuditService auditSvc, [FromServices] IUserService userSvc)
        {

            var authReturn = await adSvc.Auth(auth);

            if (!authReturn.Success)
                return Ok(authReturn);

            var authUser = await userSvc.AuthUser(auth.User, auth.Password);

            if (!authUser.Success)
                return Ok(authUser);

            if (!string.IsNullOrEmpty(auth.Token))
            {
                var ckToken = tokenSvc.CheckToken(auth.Token);
                if (!ckToken.Success && ckToken.ErrorCode == 1300)
                    return Ok(ckToken);
            }

            var newToken = await tokenSvc.GenerateToken(auth.User);

            if (!newToken.Success)
            {
                if (newToken.ErrorCode == 500)
                    return StatusCode(500, newToken);

                return Ok(newToken);
            }

            auth.Token = newToken.Token;
            var passwordUpdated = await userSvc.UpdatePasswordFromAdAuth(auth);

            if (!passwordUpdated.Success)
                return Ok(passwordUpdated);

            var ip = GetIp();

            var log = await auditSvc.CreateLogonLog(newToken.Token, ip);

            if (!log.Success)
                return Ok(log);

            authReturn.Token = newToken.Token;
            authReturn.ExpiresOn = newToken.ExpiresOn;


            return Ok(authReturn);
        }


        private string GetIp()
        {
            return _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
        }

    }
}