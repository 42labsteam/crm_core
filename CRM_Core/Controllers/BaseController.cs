﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Filters;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace CRM_Core.Controllers
{
    [ServiceFilter(typeof(CustomActionFilter))]
    [Route("api/v1/[controller]")]
    [EnableCors("MyPolicy")]
    public abstract class BaseController : Controller
    {
        protected string GetControllerName(ControllerContext context)
        {
            var name = string.Empty;
            name = context.RouteData.Values["Controller"].ToString().ToLower();

            if (name.Contains("retrieve"))
                name = "retrieve";

            return name;
        }

    
    }

   
}