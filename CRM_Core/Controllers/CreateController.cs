﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using CRM_Core.SharedKernel;
using Microsoft.AspNetCore.Mvc;

namespace CRM_Core.Controllers
{
    public class CreateController : BaseController
    {


        [HttpPost]
        public async Task<IActionResult> Create([FromBody] InputData input, [FromServices] IGenericService svc)
        {
            var result = await svc.Create(input);

            if (!result.Success)
                return Ok(result);

            result.Success = true;

            return Ok(result);
        }
    }
}