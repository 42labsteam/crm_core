﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Config;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using Microsoft.AspNetCore.Mvc;

namespace CRM_Core.Controllers
{
    public class DataBaseController : BaseController
    {
        private readonly IDataBaseService _svc;

        public DataBaseController(IDataBaseService svc)
        {
            _svc = svc;
        }

        [HttpPost("table_names")]
        public async Task<IActionResult> TableNames(BaseInput obj)
        {

            var result = await _svc.GetAllTableNames();

            return Ok(result);
        }
    }
}