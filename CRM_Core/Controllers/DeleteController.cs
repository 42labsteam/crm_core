﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using Microsoft.AspNetCore.Mvc;

namespace CRM_Core.Controllers
{

    public class DeleteController : BaseController
    {
        [HttpPost]
        public async Task<IActionResult> Delete([FromBody] InputData input, [FromServices] IGenericService svc)
        {
            var result = await svc.Delete(input);

            if (!result.Success)
                return Ok(result);

            result.Success = true;

            return Ok(result);
        }
    }
}