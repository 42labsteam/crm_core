﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Application;
using CRM_Core.Domain.Enums;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using Microsoft.AspNetCore.Mvc;

namespace CRM_Core.Controllers
{
    public class RegisterPluginController : BaseController
    {
        private IFileService _svc;

        public RegisterPluginController(IFileService svc)
        {
            _svc = svc;
        }

        [HttpPost]
        public async Task<IActionResult> RegisterPlugin([FromBody] PluginModel input)
        {
            var result = await _svc.UploadDllFile(input, "create");

            if (!result.Success)
                return Ok(result);

            return Ok(result);
        }

      

    }
}