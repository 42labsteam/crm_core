﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace CRM_Core.Controllers
{
    public class RetrieveController : BaseController
    {

        [HttpPost]
        public  async Task<IActionResult> GetById([FromBody] InputData input, [FromServices] IGenericService svc)
        {
          
            var entity = await svc.GetSingle(input);

            if (!entity.Success)
                return Ok(entity);

            return Ok(new RetrieveOutput { Success = true, Content = entity.Content });
        }
    }
}