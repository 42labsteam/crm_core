﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using Microsoft.AspNetCore.Mvc;

namespace CRM_Core.Controllers
{
    public class RetrieveMultipleController : BaseController
    {


        [HttpPost]
        public async Task<IActionResult> GetAll([FromBody] RetrieveModel input, [FromServices] IGenericService svc)
        {
           
            var result = await svc.GetAll(input);

            if (!result.Success)         
                return Ok(result);

            result.Success = true;

            return Ok(result);
        }
    }
}