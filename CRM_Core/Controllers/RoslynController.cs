﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Text;
using Microsoft.Extensions.Caching.Memory;

namespace CRM_Core.Controllers
{
    public class RoslynController : BaseController
    {
        private IMemoryCache _cache;
        private const string CACHE_KEY = "DllKey";
        private IRoslynCompilerService _svc;

        public RoslynController(IMemoryCache cache, IRoslynCompilerService svc)
        {
            _cache = cache;
            _svc = svc;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> GenerateAssembly([FromBody] GenerateAssemblyModel obj)
        {

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                var errorMsg = new List<string>();
                foreach (var error in errors)
                {
                    errorMsg.Add(error.ErrorMessage);
                }
                return Ok(errorMsg);
            }

            var action = GetControllerName(this.ControllerContext);

            var result = await _svc.GenerateAssembly(obj, action);

            if (!result.Success)
                return Ok(result);

            return Ok(result);
        }

        [HttpPost("[action]")]
        public IActionResult InvokeMethod([FromBody] InvokeMethodModel obj)
        {
            var result =  _svc.InvokeMethod(obj);

            if (!result.Success)
                return Ok(result);           

            return Ok(result);

        }
    }
}