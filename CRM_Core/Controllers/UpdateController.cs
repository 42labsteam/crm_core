﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using Microsoft.AspNetCore.Mvc;

namespace CRM_Core.Controllers
{
    public class UpdateController : BaseController
    {

        [HttpPost]
        public async Task<IActionResult> Uptade([FromBody] InputData input, [FromServices] IGenericService svc)
        {
            var result = await svc.Update(input);

            if (!result.Success)
                return Ok(result);

            result.Success = true;

            return Ok(result);

        }
    }
}