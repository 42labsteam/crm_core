﻿using CRM_Core.Application;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using CRM_Core.SharedKernel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CRM_Core.Filters
{
    public class CustomActionFilter : IAsyncActionFilter
    {
        private IFileService fileSvc;
        private ITokenService tokenSvc;
        private IGenericService svc;
        private IPrivilegeService privSvc;
        private IRoslynCompilerService roslyn;


        public CustomActionFilter(IFileService fileService, ITokenService tokenService,
            IGenericService genericService, IPrivilegeService privilegeService, IRoslynCompilerService roslynCompilerService)
        {
            fileSvc = fileService;
            tokenSvc = tokenService;
            svc = genericService;
            privSvc = privilegeService;
            roslyn = roslynCompilerService;

        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {

            try
            {
                // Get the controller name and consider that as the operation name
                var operation = context.RouteData.Values["Controller"].ToString();

                if (operation.ToLower().Contains("retrieve"))
                {
                    operation = "retrieve";
                }

                dynamic input = new ExpandoObject();

                // Verify if context contains a InputData variable called 'input' and check the authorizes
                if (context.ActionArguments.ContainsKey("input"))
                {
                    input = context.ActionArguments["input"];
                    // Check if the tokes is valid

                    if (input.EntityName.ToLower().Equals("user") && operation.ToLower().Equals("create") && input.Token == null)
                    {

                    }
                    else
                    {
                        var tokenAuth = tokenSvc.CheckToken(input.Token);

                        if (!tokenAuth.Success)
                        {
                            context.Result = new ObjectResult(tokenAuth);
                            ErrorCatcher.ClearErrors();
                            return;
                        }

                        if (input.Operation == null)
                            input = new InputData(input.EntityId, input.EntityName, operation, input.Fields, input.Token);

                        // Check if the user has authorization for this action
                        var privilege = await privSvc.ValidatePrivilege(input);

                        if (!privilege.Success)
                        {
                            context.Result = new ObjectResult(privilege);
                            ErrorCatcher.ClearErrors();
                            return;
                        }

                        // Retrieve the associated DLLs with 'PRE' action
                        var preDlls = await fileSvc.GetDllsByOperation(operation, input.EntityName, 1);

                        if (preDlls == null)
                        {
                            ErrorCatcher.AddError("Error while retrieving the 'PRE' DLLs");
                            context.Result = new ObjectResult(new BaseOutput { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() });
                            ErrorCatcher.ClearErrors();
                            return;
                        }

                        var preExecution = new BaseOutput();

                        // Execute the associated DLLs with 'PRE' action
                        if (input.GetType() == typeof(InputData))
                        {
                            preExecution = roslyn.ExecuteDlls(input, preDlls);
                        }
                        else
                        {
                            preExecution = roslyn.ExecuteDlls(input, preDlls);
                        }

                        if (!preExecution.Success)
                        {
                            context.Result = new ObjectResult(preExecution);
                            ErrorCatcher.ClearErrors();
                            return;

                        }
                    }

                    await next();
                }
                else
                {
                    await next();
                }


                // Verify if context contains a InputData variable called 'input' and execute the associated DLLs with 'POST' action
                if (context.ActionArguments.ContainsKey("input"))
                {
                    var postDlls = await fileSvc.GetDllsByOperation(operation, input.EntityName, 2);

                    if (postDlls == null)
                    {
                        ErrorCatcher.AddError("Error while retrieving the 'POST' DLLs");
                        context.Result = new ObjectResult(new BaseOutput { Success = false, ErrorCode = 500, ErrorMessage = ErrorCatcher.SetErrors() });
                        ErrorCatcher.ClearErrors();
                        return;
                    }

                    var postExecution = roslyn.ExecuteDlls(input, postDlls);

                    if (!postExecution.Success)
                    {
                        context.Result = new ObjectResult(postExecution);
                        ErrorCatcher.ClearErrors();
                        return;
                    }
                }

                ErrorCatcher.ClearErrors();
            }
            catch (Exception ex)
            {
                string innerEx = ex.InnerException == null ? string.Empty : $" - {ex.InnerException.Message}";
                context.Result = new ObjectResult(new BaseOutput { ErrorCode = 500, ErrorMessage = $"CustomActionFilter Error: {ex.Message} {innerEx}", Success = false });
                ErrorCatcher.ClearErrors();
                return;
            }


        }

    }
}

