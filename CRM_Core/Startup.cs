﻿using CRM_Core.Config;
using CRM_Core.Domain.Models;
using CRM_Core.SharedKernel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;

namespace CRM_Core
{

    public class Startup
    {
        private IConfiguration _config { get; }
        private IServiceCollection _services;
        private IConfigurationRoot ConfigRoot;


        public Startup(IConfiguration configuration)
        {
            _config = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            _services = services;
            _services.AddSingleton<IConfiguration>(_config);
            _services.AddCors();
            _services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            _services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            _services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("MyPolicy"));
            });
            _services = services;
            ServicesConfig.Configure(_services);
            DataBaseHelper.ConfigRoot = GetConfigRoot();
            StartupMethods.ExecuteMethods();
            //ClientConfig.Configure(_services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCors("MyPolicy");

            app.Use(async (context, next) =>
           {
               await next();
               if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
               {
                   context.Request.Path = "/index.html";
                   await next();
               }
           })
           .UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new List<string> { "index.html" } })
           .UseStaticFiles()
           .UseMvc();
        }


        private IConfigurationRoot GetConfigRoot()
        {
            try
            {
                var builder = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json");

                ConfigRoot = builder.Build();


                return ConfigRoot;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
