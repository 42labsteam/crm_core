﻿using CRM_Core.Application;
using CRM_Core.Domain.Enums;
using CRM_Core.Domain.Models;
using CRM_Core.Domain.Service;
using CRM_Core.Repository;
using CRM_Core.SharedKernel;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace RoslynCore
{
    public class RoslynCompiler : IRoslynCompilerService
    {

        private IMemoryCache _cache;
        private IFileService _fileSvc;
        private const string CACHE_KEY = "DllKey";
        private List<DownloadDllOutput> dlls;

        public RoslynCompiler(IMemoryCache cache, IFileService fileSvc)
        {
            _cache = cache;
            _fileSvc = fileSvc;
        }

        public async Task<BaseOutput> GenerateAssembly(GenerateAssemblyModel input, string action)
        {
            string issue = string.Empty;

            // Code syntax
            string code = input.Code;

            var tree = SyntaxFactory.ParseSyntaxTree(code);
            // dll file name
            string fileName = input.DllName.ToLower();

            // Detect the file location for the library that defines the object type
            var assemblyPath = Path.GetDirectoryName(typeof(object).Assembly.Location);

            // Create a reference to the library
            var refs = SetReferences(assemblyPath);

            // A single, immutable invocation to the compiler
            // to produce a library
            var compilation = CSharpCompilation.Create(fileName)
              .WithOptions(
                new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
              .AddReferences(refs)
              .AddSyntaxTrees(tree);

            using (var stream = new MemoryStream())
            {
                var emitResult = compilation.Emit(stream);

                if (emitResult.Success)
                {
                    Assembly assembly = Assembly.Load(stream.ToArray());
                    // Transform the dll file into a byte[]
                    var bytes = stream.ToArray();
                    var base64string = Convert.ToBase64String(bytes);

                    var pluginModel = new PluginModel { Base64StringDll = base64string, EntityName = input.EntityName, InvokeAt = input.InvokeAt, Operation = input.Operation, Token = input.Token, FileName = input.DllName };
                    var uploadedObj = await _fileSvc.UploadDllFile(pluginModel, action);

                    if (!uploadedObj.Success)
                        return uploadedObj;

                    return new BaseOutput { Success = true };
                }
                else
                {
                    // Search for any erros in code syntax
                    foreach (Diagnostic codeIssue in emitResult.Diagnostics)
                    {
                        issue = $@"ID: {codeIssue.Id}, Message: {codeIssue.GetMessage()},
                    Location: { codeIssue.Location.GetLineSpan()},
                    Severity: { codeIssue.Severity}
                    ";
                    }
                    return new BaseOutput { Success = false, ErrorCode = 400, ErrorMessage = issue };

                }
            }

        }

        public InvokeMethodOutput InvokeMethod(InvokeMethodModel input)
        {
            try
            {

                object result = null;
                DownloadDllOutput obj = new DownloadDllOutput();

                // Check if th MemoryCache key 'CACHE_KEY' contains a value
                if (_cache.TryGetValue<List<DownloadDllOutput>>(CACHE_KEY, out dlls))
                {
                    // Load the assembly where contains an specific name
                    obj = dlls.Where(a => a.FileName.ToLower().Equals(input.AssemblyName.ToLower().Replace(".dll", ""))).FirstOrDefault();

                    if (obj == null)
                        return new InvokeMethodOutput { Success = false, ErrorCode = 400, ErrorMessage = $"File {input.AssemblyName.ToLower()} not found" };

                    Assembly assembly = obj.Assembly;
                    // Invoke the class method passing an argument
                    // Ex: asm.GetType("RoslynCore.Helper").GetMethod("CalculateCircleArea").
                    result = assembly.GetType($"{input.NameSpace}.{input.ClassName}").GetMethod(input.Method).Invoke(null, input.Parameters);
                }

               

                return new InvokeMethodOutput { Success = true, Result = result };
            }
            catch (Exception ex)
            {

                return new InvokeMethodOutput { Success = false, ErrorCode = 400, ErrorMessage = ex.Message };
            }
        }


        public BaseOutput ExecuteDlls(InputData input, IEnumerable<DownloadDllOutput> dlls)
        {

            try
            {
                object[] parameters = new object[] { input };

                foreach (var dll in dlls)
                {
                    var model = SetInvokeMethodProperties(dll.Assembly);
                    var methods = GetDllMethods(model.AssemblyName, dll.Assembly);

                    foreach (var method in methods)
                    {
                        model.Method = method.Name;
                        model.Parameters = parameters;
                        var invokeModel = InvokeMethod(model);

                        if (!invokeModel.Success)
                            return invokeModel;

                        dynamic result = invokeModel.Result;

                        if (result.GetType() == typeof(BaseOutput))
                        {
                            BaseOutput output = result as BaseOutput;

                            if (!output.Success)
                                return output;
                        }
                        else if (result.Result.GetType() == typeof(BaseOutput))
                        {
                            BaseOutput output = result.Result as BaseOutput;

                            if (!output.Success)
                                return output;
                        }

                    }
                }


                return new BaseOutput { Success = true };
            }
            catch (Exception ex)
            {

                return new BaseOutput { Success = false, ErrorCode = 400, ErrorMessage = ex.Message };
            }

        }

        private InvokeMethodModel SetInvokeMethodProperties(Assembly assembly)
        {
            var model = new InvokeMethodModel();
            model.AssemblyName = assembly.Modules.FirstOrDefault().ScopeName;
            model.NameSpace = assembly.DefinedTypes.FirstOrDefault().Namespace;
            model.ClassName = assembly.DefinedTypes.FirstOrDefault().Name;

            return model;
        }


        private IEnumerable<MethodInfo> GetDllMethods(string scopeName, Assembly assembly)
        {
            var methods = assembly.DefinedTypes.FirstOrDefault().GetMethods();
            var methodList = methods.ToList();
            var resultList = methods.Where(m => m.Module.ScopeName == scopeName);

            return resultList;
        }


        /// <summary>
        ///     Configure the necessary references. Ex: using System; using CRM_Core.Domain.Models;
        /// </summary>
        /// <param name="assemblyPath"></param>
        /// <returns></returns>
        private IEnumerable<PortableExecutableReference> SetReferences(string assemblyPath)
        {

            DirectoryInfo di = new DirectoryInfo(assemblyPath);
            List<PortableExecutableReference> refs = new List<PortableExecutableReference>();

            FileInfo[] rgFiles = di.GetFiles("*.dll");

            foreach (FileInfo fi in rgFiles)
            {
                if (!fi.Name.Contains("api-ms-win") && !fi.Name.Contains("ucrtbase.dll") && !fi.Name.Contains("sos") && !fi.Name.Contains("mscorrc")
                    && !fi.Name.Contains("mscordbi.dll") && !fi.Name.Contains("mscordaccore") && !fi.Name.Contains("Microsoft.DiaSymReader.Native.amd64.dll")
                    && !fi.Name.Contains("hostpolicy.dll") && !fi.Name.Contains("dbgshim.dll") && !fi.Name.Contains("coreclr.dll") && !fi.Name.Contains("clrjit.dll")
                    && !fi.Name.Contains("clretwrc.dll") && !fi.Name.Contains("clrcompression.dll"))
                    refs.Add(MetadataReference.CreateFromFile(Path.Combine(assemblyPath, fi.Name)));
            }

        
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(object).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(InputData).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(GenericService).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(GenericRepository).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(DataBaseHelper).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(Microsoft.Exchange.WebServices.Data.EmailMessage).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(ComparisonOperators).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Path.Combine(typeof(Newtonsoft.Json.JsonConvert).Assembly.Location)));
            refs.Add(MetadataReference.CreateFromFile(Assembly.GetEntryAssembly().Location));

            return refs;
        }


        //private IEnumerable<string> GetProjectsPath()
        //{
        //    List<string> paths = new List<string>();

        //    paths.Add(typeof(InputData).Assembly.Location);
        //    paths.Add(typeof(GenericService).Assembly.Location);
        //    paths.Add(typeof(GenericRepository).Assembly.Location);
        //    paths.Add(typeof(DataBaseHelper).Assembly.Location);
        //    paths.Add(typeof(RoslynCompiler).Assembly.Location);

        //    return paths;
        //}

        //private string GetProjectByName(string projName)
        //{

        //    var project = string.Empty;
        //    var projects = GetProjectsPath();
        //    project = projects.Where( x => x.Contains(projName)).FirstOrDefault();

        //    return project;

        //}
    }
}
